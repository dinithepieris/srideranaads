package com.mobisecsolutions.dinithepieris.addswallet.Common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.widget.EditText;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Activity.LoginActivity;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.Model.DeliveredText;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.mobisecsolutions.dinithepieris.addswallet.BuildConfig;
import com.mobisecsolutions.dinithepieris.addswallet.R;


/**
 * Created by dinithe pieris on 8/1/2017.
 */

public class Common {

    private static final String CONTACT_ID = ContactsContract.Contacts._ID;
    private static final String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
    private static final String PHONE_NUMBER_NAME = ContactsContract.Contacts.DISPLAY_NAME;

    private static final String PHONE_NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
    private static final String PHONE_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
    private static final String PHONE_CONTACT_EMAIL = ContactsContract.CommonDataKinds.Email.DATA;

    public static final int SMS_TIME_OUT = 1000*8;
    public static final int SMS_DELIVERY_TIME_OUT = 1000*6;


    public static int APPONLINE = 1;
    public static int APPOFFLINE = 2;

    public static String ADVERTISER = "10";

    public static String PUBLISHER = "20";

    public static int DaysOfWeek = 7;
    public static int DaysOfMonth = 30;

    public static int LOADPROFILE = 1;

    public static int UPDATEPROFILE= 0;

    public static int AppMode;

    public static int RequestTimeOut = 40*1000;

//    public static String lstselect;

    public static String token;

    public static boolean IS_CONTACTS_SYNC;

    public static int getAppMode() {
        return AppMode;
    }

    public static void setAppMode(int appMode) {
        AppMode = appMode;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        Common.token = token;
    }


    public static ArrayList<ContactModel> myContacts;

    public static ArrayList<ContactModel> getMyContacts() {
        return myContacts;
    }

    public static void setMyContacts(ArrayList<ContactModel> myContacts) {
        Common.myContacts = myContacts;
    }

    public static ArrayList<DeliveredText> DeliSmsList;

    public static ArrayList<DeliveredText> getDeliSmsList() {
        return DeliSmsList;
    }

    public static void setDeliSmsList(ArrayList<DeliveredText> deliSmsList) {
        DeliSmsList = deliSmsList;
    }


    /**
     * check google play services
     **/
//    public boolean isGooglePlayServicesAvailable(Activity activity) {
//        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
//        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
//        if(status != ConnectionResult.SUCCESS) {
//            if(googleApiAvailability.isUserResolvableError(status)) {
//                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
//            }
//            return false;
//        }
//        return true;
//    }

    /**
     * get device id
     */
    public static String getAndroidDeviceId(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        return deviceId;
    }

    /**
     * check internet services
     **/
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * check internet services check
     **/
    public static boolean isNetworkAvailableCheck(final Context context) {
        if(!isNetworkAvailable(context)){
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle(context.getResources().getString(R.string.app_name));
            alertDialog.setMessage(context.getResources().getString(R.string.networkenable));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent=new Intent(Settings.ACTION_SETTINGS);
                            context.startActivity(intent);
                        }
                    });
            alertDialog.show();
            alertDialog.setCancelable(false);

        }
        return true;
    }



    /**
     * check internet network:
     **/
    public static boolean isInternetAvailable() {
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }

    /**
     * check GPS Service:
     **/
    public static void showGPSDisabledAlertToUser(final Activity activity) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    /*
    *validation show dialog
    * */
    public static void validationDialog(Context context, String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(context.getResources().getString(R.string.app_name));
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    /*
    *show dialog
    * */
    public static void showDialogAlertOK(Context context, String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

       /*
       *show dialog
       * */

    public static void showDialogDirecttoLogin(final Context context, String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent=  new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                });
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

           /*
       *show dialog
       * */

    public static void showDialogDirect(final Context context, String title, String msg, final String nextActiviry ) {

         Class<?> myClass = null;
        try {
            String myclasspath = "com.mobisecsolutions.dinithepieris.addswallet.Activity."+nextActiviry;
            myClass = Class.forName(myclasspath);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        final Class<?> finalMyClass = myClass;
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent=  new Intent(context, finalMyClass);
                        context.startActivity(intent);
                        alertDialog.dismiss();
                    }
                });
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    /*
     *check appverstion
     */
    public static void AppVersionCheck(final Context context, int appVCode) {

        int verCode = BuildConfig.VERSION_CODE;

        if (verCode<appVCode){

            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle(context.getResources().getString(R.string.app_name));
            alertDialog.setMessage("New application version released!\nPlease update your application to latest version to get more Ads!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            final String appPackageName = "com.mobisecsolutions.dinithepieris.addswallet"; // getPackageName() from Context or Activity object
                            try {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });
            alertDialog.show();
            alertDialog.setCancelable(false);

        }
    }


    public  static ArrayList<ContactModel> getDeviceContacts(Context context){

        ArrayList<ContactModel> contactList = new ArrayList<>();
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        while (phones.moveToNext())
        {

            ContactModel info = new ContactModel();

            //String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            //String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            info.name = phones.getString(phones.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)).replaceAll("[^a-zA-Z0-9 ]", "").trim();
            String conNum = numValidateFormatter(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            info.mobileNumber = conNum;
            info.isSelected = false;

            if(!conNum.equalsIgnoreCase("0")&&!contains(contactList,conNum)) {
                contactList.add(info);
            }

        }
        phones.close();

        return contactList;
    }


    //get contacts
    public static ArrayList<ContactModel> getContacts(Context ctx) {
        ArrayList<ContactModel> contactList = new ArrayList<>();
        ContentResolver contentResolver = ctx.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
//                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(),
//                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));
//
//                    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id));
//                    Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
//
//                    Bitmap photo = null;
//                    if (inputStream != null) {
//                        photo = BitmapFactory.decodeStream(inputStream);
//                    }

                    while (cursorInfo.moveToNext()) {
                        ContactModel info = new ContactModel();
                        //info.id = id;
                        //info.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        //info.mobileNumber = cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //info.email = "test@email.com";//cursor.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        //info.photo = photo;
                        //info.photoURI= pURI;
                        //info.isSelected = false;

                        String conNum = numValidateFormatter(cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));

                        if(!conNum.equalsIgnoreCase("0")){


                            info.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)).replaceAll("[^a-zA-Z0-9 ]", "").trim();
                            info.mobileNumber = conNum.trim();
                            info.isSelected = false;

                            if(!contains(contactList,conNum)) {
                                contactList.add(info);
                            }
                        }
                    }
                    cursorInfo.close();
                }
            }
        }

        return contactList;
    }

    private static boolean contains(ArrayList<ContactModel> list, String number) {
        for (ContactModel item : list) {
            if (item.getMobileNumber().equals(number)) {
                return true;
            }
        }
        return false;
    }

    private static  String numValidateFormatter(String conNum) {

        conNum = conNum.replaceAll(" ","")
                .replaceAll("-","")
                .replaceAll("\\(","")
                .replaceAll("\\)","");

        if (conNum.length() == 10 && conNum.charAt(0) == '0' && conNum.charAt(1) == '7') {
            return conNum;
        } else if (conNum.length() == 9 && conNum.charAt(0) == '7') {
            return conNum = '0' + conNum;
        } else if (conNum.length() == 12 && conNum.charAt(0) == '+' && conNum.charAt(1) == '9'
                && conNum.charAt(2) == '4' && conNum.charAt(3) == '7') {
            return conNum = conNum.replace("+94","0");
        }
        return "0";
    }

    public static String getDateToday(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setLenient(false);
        String formattedDate = dateFormatter.format(c.getTime());

        return formattedDate;
    }

    public static String getDateTime(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatter.setLenient(false);
        String formattedDate = dateFormatter.format(c.getTime());

        return formattedDate;
    }


    public static String getDatePreviousday(int cntdays) {

        Calendar calendar = Calendar.getInstance(); // this would default to now
        calendar.add(Calendar.DAY_OF_MONTH, - cntdays);

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setLenient(false);
        String formattedDate = dateFormatter.format(calendar.getTime());

        return formattedDate;

    }

    public static void listviewSelector(Context context, final String[] items, final EditText edttxt){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Make your selection :");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                //lstselect =  items[item];
                edttxt.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

        //return lstselect;
    }

    public static void listviewSelector(final Context context, final String[] items){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Make your selection :");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                //lstselect =  items[item];
                Toast.makeText(context," Selected language : " + items[item],Toast.LENGTH_LONG).show();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        //return lstselect;
    }

    public static int  getDefaultSimmm(Context context) {

        Object tm = context.getSystemService(Context.TELEPHONY_SERVICE);
        Method method_getDefaultSim;
        int defaultSimm = -1;
        try {
            method_getDefaultSim = tm.getClass().getDeclaredMethod("getDefaultSim");
            method_getDefaultSim.setAccessible(true);
            defaultSimm = (Integer) method_getDefaultSim.invoke(tm);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Method method_getSmsDefaultSim;
        int smsDefaultSim = -1;
        try {
            method_getSmsDefaultSim = tm.getClass().getDeclaredMethod("getSmsDefaultSim");
            smsDefaultSim = (Integer) method_getSmsDefaultSim.invoke(tm);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return smsDefaultSim;
    }



//    public static void sendSMS(final Context context, String phoneNumber, String message, String id) {
//        String SENT = "SMS_SENT"+"_"+id+"_"+phoneNumber;
//        String DELIVERED = "SMS_DELIVERED"+"_"+id+"_"+phoneNumber;
//
//        Intent sentIntent = new Intent(SENT);
//        sentIntent.putExtra("no", phoneNumber);
//        sentIntent.putExtra("id", id);
//        //sentIntent.putExtra("SIM", getNumberOfActiveSim(context));
//
//        Intent deliveryIntent = new Intent(DELIVERED);
//        deliveryIntent.putExtra("no", phoneNumber);
//        deliveryIntent.putExtra("id", id);
//        //deliveryIntent.putExtra("SIM", getNumberOfActiveSim(context));
//
////        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
////        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveryIntent, 0);
//
//
//        //---when the SMS has been sent---
//        context.registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(context.getApplicationContext(), "SMS sent", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(context.getApplicationContext(), "Generic failure", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(context.getApplicationContext(), "No service", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(context.getApplicationContext(), "Null PDU", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(context.getApplicationContext(), "Radio off", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        },new IntentFilter(SENT));
//
//        //---when the SMS has been delivered---
//        context.registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        DeliveredText deliveredText = new DeliveredText();
//                        Toast.makeText(context.getApplicationContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
//                        String no = intent.getStringExtra("no");
//                        String id = intent.getStringExtra("id");
//                        deliveredText.setPhoneNumber(no);
//                        deliveredText.setAdId(id);
//
//                        ArrayList<DeliveredText> deliveredTextList = new ArrayList<DeliveredText>();
//                        //setDeliSmsList(deliveredTextList);
//
//                        Session session = new Session(context);
//                        deliveredTextList.addAll(0,session.getDeliveredList());
//                        deliveredTextList.add(deliveredText);
//                        session.delDeliveredList();
//                        session.setDeliveredList(deliveredTextList);
//
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(context.getApplicationContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(DELIVERED));
//
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
//    }

//    public static int getNumberOfActiveSim(Context context) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
//            SubscriptionManager subscriptionManager = SubscriptionManager.from(context);
//            List<SubscriptionInfo> subscriptionInfo = subscriptionManager.getActiveSubscriptionInfoList();
//            return subscriptionInfo != null ? subscriptionInfo.size() : 0;
//        }
//        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        if(telephonyManager != null && telephonyManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT){
//            return 1;
//        }
//        return 0;
//    }

    public static void sendSMSNew(final Context context,  String phoneNumber, String message, String id) {

        BroadcastReceiver sendBroadcastReceiver;
        BroadcastReceiver deliveryBroadcastReceiver;

        String SENT = "SMS_SENT"+"_"+id+"_"+phoneNumber;
        String DELIVERED = "SMS_DELIVERED"+"_"+id+"_"+phoneNumber;

        Intent sentIntent = new Intent(SENT);
        sentIntent.putExtra("no", phoneNumber);
        sentIntent.putExtra("id", id);
        //sentIntent.putExtra("SIM", 1);

        Intent deliveryIntent = new Intent(DELIVERED);
        deliveryIntent.putExtra("no", phoneNumber);
        deliveryIntent.putExtra("id", id);
        //deliveryIntent.putExtra("SIM", 1);

//        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent( SENT), 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveryIntent, 0);

        // ---when the SMS has been sent---
        context.registerReceiver(sendBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
//                        ContentValues values = new ContentValues();
//                        for (int i = 0; i < MobNumber.size() - 1; i++) {
//                            values.put("address", MobNumber.get(i).toString());// txtPhoneNo.getText().toString());
//                            values.put("body", MessageText.getText().toString());
//                        }
//                        context.getContentResolver().insert( Uri.parse("content://sms/sent"), values);

                        Toast.makeText(context, "SMS sent", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }

        }, new IntentFilter(SENT));

        // ---when the SMS has been delivered---
        context.registerReceiver(deliveryBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(context, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }

        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

        //context.unregisterReceiver(sendBroadcastReceiver);
        //context.unregisterReceiver(deliveryBroadcastReceiver);
    }


    public static void backPressAlert(final Context context){
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Sri Derana Ads")
                .setMessage("Are you sure to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        //context.finish();

                        System.exit(0);
                    }

                })
                .setNegativeButton("No", null)
                .show();


    }




//    private void sendSMS(String phoneNumber, String message,String id) {
//        String SENT = "SMS_SENT"+phoneNumber+id;
//        String DELIVERED = "SMS_DELIVERED"+phoneNumber+id;
//
//        Intent sentIntent = new Intent(SENT);
//        sentIntent.putExtra("no", phoneNumber);
//        sentIntent.putExtra("id", id);
//
//        Intent deliveryIntent = new Intent(DELIVERED);
//        deliveryIntent.putExtra("no", phoneNumber);
//        deliveryIntent.putExtra("id", id);
//
//        //PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent( SENT), 0);
//        //PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, sentIntent, 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, deliveryIntent, 0);
//
//        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));
//        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
//
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
//
//    }
//
//    class DeliverReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            switch (getResultCode()) {
//                case Activity.RESULT_OK:
//                    DeliveredText deliveredText = new DeliveredText();
//                    Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
//                    String no = intent.getStringExtra("no");
//                    String id = intent.getStringExtra("id");
//                    deliveredText.setPhoneNumber(no);
//                    deliveredText.setAdId(id);
//                    deliveredTextList.add(deliveredText);
//                    Common.setDeliSmsList(deliveredTextList);
//                    break;
//                case Activity.RESULT_CANCELED:
//                    Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
//                    break;
//            }
//
//        }
//    }
//
//    class SentReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent arg1) {
//            switch (getResultCode()) {
//                case Activity.RESULT_OK:
//                    //Toast.makeText(getBaseContext(), sms_sent, Toast.LENGTH_SHORT) .show();
//                    //startActivity(new Intent(SendSMS.this, ChooseOption.class));
//                    //overridePendingTransition(R.anim.animation, R.anim.animation2);
//                    Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                    Toast.makeText(getBaseContext(), "Generic failure",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_NO_SERVICE:
//                    Toast.makeText(getBaseContext(), "No service",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_NULL_PDU:
//                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
//                            .show();
//                    break;
//                case SmsManager.RESULT_ERROR_RADIO_OFF:
//                    Toast.makeText(getBaseContext(), "Radio off",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//            }
//
//        }
//    }

}
