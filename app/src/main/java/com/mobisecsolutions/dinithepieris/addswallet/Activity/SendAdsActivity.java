package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.ContactListAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.Model.DeliveredText;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;
import com.mobisecsolutions.dinithepieris.addswallet.Service.TelephonyInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 8/23/17.
 */

public class SendAdsActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    ArrayList<ContactModel> contactModelList = null;
    ListView lstv_contact;
    String urladsendcontactlist = "/api/adsendcontactlist";
    String urladdeliveredcontactlist = "/api/addeliveredcontactlist";
    String txtAd;
    String txtAd_id;
    int SMSCnt;
    Session session;
    boolean runningThread = false;

    public static CheckBox chkbx_selectAll;

    ContactListAdapter contlistAdapter;

    private static BroadcastReceiver sendBroadcastReceiver;
    private static BroadcastReceiver deliveryBroadcastReceiver;

    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    final int REQUEST_READ_PHONE_STATE = 100;

    JSONArray jobjarrContsSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_ads);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                txtAd = null;
            } else {
                txtAd = extras.getString("TxtAd");
                txtAd_id = extras.getString("TxtAdID");
                SMSCnt = extras.getInt("SMSCnt");
            }
        } else {
            txtAd = (String) savedInstanceState.getSerializable("TxtAd");
            txtAd_id = (String) savedInstanceState.getSerializable("TxtAdID");
            SMSCnt = (int) savedInstanceState.getSerializable("SMSCnt");
        }

        getSupportActionBar().setTitle(getResources().getString(R.string.sendAds));

        init();
    }

    private void init() {

        session = new Session(SendAdsActivity.this); //in oncreate
        ActivityCompat.requestPermissions(SendAdsActivity.this, new String[]{Manifest.permission.SEND_SMS}, 1);

        lstv_contact = (ListView) findViewById(R.id.lstv_contact);
        contactModelList = new ArrayList<ContactModel>();
        //contactModelList = getContacts(this);

        contactModelList = Common.getMyContacts();

        if (contactModelList.size() > 0) {
            //ContactListArrayAdapter contlistAdapter; = new ContactListArrayAdapter(SendAdsActivity.this, contactModelList);
            //lstv_contact.setAdapter(contlistAdapter);

            contlistAdapter = new ContactListAdapter(SendAdsActivity.this, contactModelList);
            lstv_contact.setAdapter(contlistAdapter);
        }

        chkbx_selectAll = (CheckBox) findViewById(R.id.chkbx_selectAll);
//        chkbx_selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean select) {
//                if(select){
//
//
//                    //txtv_selecnumber.setText("Selected: "+ADS_selcted_cnt);
//                    for(int i=0; i < contactModelList.size(); i++){
//                        contactModelList.get(i).setisSelected(true);
//                    }
//
////                }else {
////                    ADS_selcted_cnt = 0;
////                    txtv_selecnumber.setText("Selected: "+ADS_selcted_cnt);
////
////                    for(int i=0; i < contactModelList.size(); i++){
////                        contactModelList.get(i).setisSelected(false);
////                    }
//                }
//                contlistAdapter.notifyDataSetChanged();
//            }
//        });

        chkbx_selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chkbx_selectAll.isChecked()) {

                    for (int i = 0; i < contactModelList.size(); i++) {
                        contactModelList.get(i).setisSelected(true);
                    }
                    contlistAdapter.notifyDataSetChanged();

                } else {

                    for (int i = 0; i < contactModelList.size(); i++) {
                        contactModelList.get(i).setisSelected(false);
                    }
                    contlistAdapter.notifyDataSetChanged();
                }

            }
        });


        Button btn_send = (Button) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callSendSMS();


            }
        });

        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (session.getDeliveredList().size() > 0) {
                    callUpdateDeliveryTask();
                } else {
                    Intent i = new Intent(SendAdsActivity.this, AdsMainActivity.class);
                    i.putExtra("state", Common.getAppMode());
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    private void callSendSMS() {
        if (contactModelList != null) {
            JSONObject jsonObject = new JSONObject();
            final JSONArray jsonArray = new JSONArray();
            try {

                jsonObject.accumulate("token", Common.getToken());
                jsonObject.accumulate("ad_id", txtAd_id);

                for (ContactModel contact : contactModelList) {

                    if (contact.isSelected) {

                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("name", contact.getName());
                        jsonObj.put("msisdn", contact.getMobileNumber());
                        jsonObj.put("email", contact.getEmail());
                        jsonArray.put(jsonObj);

                        //new SendSMSAsyncTask().execute(contact.getMobileNumber());
                    }
                }

                jsonObject.accumulate("sent_contact_list", jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (jsonArray.length() == 0) {

                Toast.makeText(SendAdsActivity.this, getResources().getText(R.string.Pleaseselectcontactstosendsms), Toast.LENGTH_LONG).show();

            } else if (isContactListExceeds(jsonArray.length())) {

                Toast.makeText(SendAdsActivity.this, getResources().getText(R.string.Youhaveexceededtheexpectedsmscount), Toast.LENGTH_LONG).show();

            } else if (Common.isNetworkAvailableCheck(SendAdsActivity.this)) {

                new SyncSentSMSContactsTask(jsonArray.length()).execute(jsonObject.toString());

            }
        }
    }

    private boolean isContactListExceeds(int i) {
        if (i > SMSCnt) {
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (session.getDeliveredList().size() > 0) {
            callUpdateDeliveryTask();
        } else {
            Intent i = new Intent(SendAdsActivity.this, AdsMainActivity.class);
            i.putExtra("state", Common.APPONLINE);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onPause() {
        try {
            if (sendBroadcastReceiver != null) {
                unregisterReceiver(sendBroadcastReceiver);
                unregisterReceiver(deliveryBroadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
        runningThread = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        runningThread = true;
    }

    @Override
    protected void onDestroy() {

        try {
            if (sendBroadcastReceiver != null) {
                unregisterReceiver(sendBroadcastReceiver);
                unregisterReceiver(deliveryBroadcastReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
        runningThread = false;
    }

//    private boolean approveSendSMS(String message){
//        final boolean state;
//        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(SendAdsActivity.this);
//        myAlertDialog.setTitle(getResources().getString(R.string.app_name));
//        myAlertDialog.setMessage(message);
//        myAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface arg0, int arg1) {
//                state = true;
//            }});
//        myAlertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface arg0, int arg1) {
//                state = false;
//            }});
//        myAlertDialog.show();
//        return state;
//    }

//    private void sendSmsCallerThread(final String num){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (true) {
//                    try {
//                        Common.sendSMS(SendAdsActivity.this, num, txtAd, txtAd_id);
//                        Thread.sleep((long) Math.floor(Common.SMS_TIME_OUT));
//                        if (!runningThread) {
//                            return;
//                        }
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();
//    }

    /**
     * Call send SMS Task
     **/


    private void callSendSMSAsyncTask(final JSONArray jsonArray, final String simId) {
        progressBar = new ProgressDialog(SendAdsActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage(getResources().getString(R.string.SMSSending));
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(jsonArray.length());
        progressBar.show();

        //reset progress bar status
        progressBarStatus = 0;

        new Thread(new Runnable() {
            public void run() {
                while (progressBarStatus < jsonArray.length()) {

                    // process some tasks
                    //progressBarStatus = doSomeTasks();

                    try {

                        //Common.sendSMS(SendAdsActivity.this, jsonArray.getString(progressBarStatus), txtAd, txtAd_id);
                        smsSend(SendAdsActivity.this, jsonArray.getString(progressBarStatus), txtAd, txtAd_id, simId);
                        progressBarStatus++;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // your computer is too fast, sleep 1 second
                    try {
                        Thread.sleep(1000 * 6);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Update the progress bar
                    progressBarHandler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }

                // ok, file is downloaded,
                if (progressBarStatus >= jsonArray.length()) {

                    // sleep 2 seconds, so that you can see the 100%
                    try {
                        Thread.sleep(1000 * 5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // close the progress bar dialog
                    progressBar.dismiss();
                }
            }
        }).start();

    }

    public void smsSend(final Context context, String phoneNumber, String txtmessage, String id, String simId) {
        phoneNumber = phoneNumber.replaceAll("[^0-9]", "").trim();
        String SENT = "SMS_SENT" + "_" + id + "_" + phoneNumber;
        String DELIVERED = "SMS_DELIVERED" + "_" + id + "_" + phoneNumber;

        Intent sentIntent = new Intent(SENT);
        sentIntent.putExtra("no", phoneNumber);
        sentIntent.putExtra("id", id);
        sentIntent.setType("vnd.android-dir/mms-sms");
        sentIntent.putExtra("sms_body", "default content");
        sentIntent.putExtra("SIM", simId);

        Intent deliveryIntent = new Intent(DELIVERED);
        deliveryIntent.putExtra("no", phoneNumber);
        deliveryIntent.putExtra("id", id);
        sentIntent.putExtra("SIM", simId);
        //deliveryIntent.putExtra("SIM", getNumberOfActiveSim(context));

//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveryIntent, 0);


        //---when the SMS has been sent---
        context.registerReceiver(sendBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context.getApplicationContext(), "SMS sent", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context.getApplicationContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context.getApplicationContext(), "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context.getApplicationContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context.getApplicationContext(), "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        context.registerReceiver(deliveryBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        DeliveredText deliveredText = new DeliveredText();
                        Toast.makeText(context.getApplicationContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        String no = intent.getStringExtra("no");
                        String id = intent.getStringExtra("id");
                        deliveredText.setPhoneNumber(no);
                        deliveredText.setAdId(id);

                        ArrayList<DeliveredText> deliveredTextList = new ArrayList<DeliveredText>();
                        //setDeliSmsList(deliveredTextList);

                        Session session = new Session(context);
                        deliveredTextList.addAll(0, session.getDeliveredList());
                        deliveredTextList.add(deliveredText);
                        session.delDeliveredList();
                        session.setDeliveredList(deliveredTextList);

                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(context.getApplicationContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        registerReceiver(deliveryBroadcastReceiver, new IntentFilter(DELIVERED));
        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        SmsManager sms = SmsManager.getDefault();
        List<String> messages = sms.divideMessage(txtmessage);
        for (String message : messages) {
            sms.sendTextMessage(phoneNumber, simId, message, sentPI, deliveredPI);
        }

        //ArrayList<String> texts = sms.divideMessage(message);
        //sms.sendMultipartTextMessage(phoneNumber, null, texts, null, null);
        //sms.getSmsManagerForSubscriptionId(1).sendTextMessage(phoneNumber, null, txtmessage, sentPI, deliveredPI);

    }

//    private void smsSend(final Context context, String phoneNumber, String message, String id) {
//
//        SENT = "SMS_SENT"+"_"+id+"_"+phoneNumber;
//        DELIVERED = "SMS_DELIVERED"+"_"+id+"_"+phoneNumber;
//
//        Intent sentIntent = new Intent(SENT);
//        sentIntent.putExtra("no", phoneNumber);
//        sentIntent.putExtra("id", id);
//        //sentIntent.putExtra("SIM", getNumberOfActiveSim(context));
//
//        Intent deliveryIntent = new Intent(DELIVERED);
//        deliveryIntent.putExtra("no", phoneNumber);
//        deliveryIntent.putExtra("id", id);
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveryIntent, 0);
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
//    }
//
//
//    private void initsmsSend() {
//
//        //---when the SMS has been sent---
//        sendBroadcastReceiver = new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        };
//
//        //---when the SMS has been delivered---
//        deliveryBroadcastReceiver =new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        DeliveredText deliveredText = new DeliveredText();
//                        Toast.makeText(context.getApplicationContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
//                        String no = intent.getStringExtra("no");
//                        String id = intent.getStringExtra("id");
//                        deliveredText.setPhoneNumber(no);
//                        deliveredText.setAdId(id);
//
//                        ArrayList<DeliveredText> deliveredTextList = new ArrayList<DeliveredText>();
//                        //setDeliSmsList(deliveredTextList);
//
//                        Session session = new Session(context);
//                        deliveredTextList.addAll(0,session.getDeliveredList());
//                        deliveredTextList.add(deliveredText);
//                        session.delDeliveredList();
//                        session.setDeliveredList(deliveredTextList);
//
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(context.getApplicationContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        };
//
//        registerReceiver(deliveryBroadcastReceiver, new IntentFilter(DELIVERED));
//        registerReceiver(sendBroadcastReceiver , new IntentFilter(SENT));
//    }

//    @Override
//    protected void onStop()
//    {
//        unregisterReceiver(sendBroadcastReceiver);
//        unregisterReceiver(deliveryBroadcastReceiver);
//        super.onStop();
//    }


////    public void callSendSMSAsyncTask(JSONObject jsonObj) {
////        showProgressDialog("Sms Sending...");
////        final Handler handler = new Handler();
////        new Thread()
////        {
////            public void run()
////            {
////                //do async operations here
////
////                handler.sendEmptyMessage(0);
////            }
////        }.start();
////
////        Handler handler = new Handler()
////        {
////
////            @Override
////            public void handleMessage(Message msg)
////            {
////                dismissProgressDialog();
////                super.handleMessage(msg);
////            }
////
////        };
////    }
//
////    private void callSendSMSAsyncTask(JSONObject jsonObj) {
////
////        try {
////            JSONArray jobjarr = jsonObj.getJSONArray("sent_contact_list");
////            for (int i = 0; i < jobjarr.length(); i++) {
////                showProgressDialog("Sending SMS...");
////
////                Common.sendSMS(SendAdsActivity.this, jobjarr.getString(i), txtAd, txtAd_id);
////
////                Handler handler = new Handler();
////                handler.postDelayed(new Runnable() {
////                    public void run() {
////                        dismissProgressDialog();
////                    }
////                }, Common.SMS_DELIVERY_TIME_OUT);
////
////            }
////
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////    }
//
////    private void callSendSMSAsyncTask(JSONObject jsonObj){
////        try {
////            JSONArray jobjarr = jsonObj.getJSONArray("sent_contact_list");
////            if ((pDialog == null) || !pDialog.isShowing()){
////                showProgressDialog("Sending SMS...");
////            }
////            for (int i = 0; i < jobjarr.length(); i++) {
////
////                Common.sendSMS(SendAdsActivity.this, jobjarr.getString(i), txtAd, txtAd_id);
////                try {
////                    Thread.sleep(Common.SMS_TIME_OUT);
////                } catch (InterruptedException e) {
////                    e.printStackTrace();
////                }
////
////                //new SendSMSAsyncTask().execute(jobjarr.getString(i));
////            }
////            if (pDialog.isShowing()) {
////                pDialog.dismiss();
////            }
////
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
////    }
//
//    /**
//     *send sms
//     ***/
//    class SendSMSAsyncTask extends  AsyncTask<String, Void, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
////            if ((pDialog == null) || !pDialog.isShowing()){
////                showProgressDialog("Sending SMS...");
////            }
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            String num = strings[0];
//            Common.sendSMS(SendAdsActivity.this, num, txtAd, txtAd_id);
//            try {
//                Thread.sleep(Common.SMS_TIME_OUT);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
////            if (pDialog.isShowing()) {
////                pDialog.dismiss();
////            }
//        }
//    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class SyncSentSMSContactsTask extends AsyncTask<String, Void, String> {

        int contactsSelected;

        public SyncSentSMSContactsTask(int length) {
            contactsSelected = length;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // Showing progress dialog
            pDialog = new ProgressDialog(SendAdsActivity.this);
            pDialog.setMessage(getResources().getString(R.string.SMSSendProgress));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();
            // Making a request to url and getting response

            String strUrl = AppConfig.getBaseUrl() + urladsendcontactlist;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    final JSONObject jsonObj = new JSONObject(result);

                    if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(SendAdsActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {

                        if (Common.isNetworkAvailableCheck(SendAdsActivity.this)) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(SendAdsActivity.this);
                            builder.setTitle(getResources().getString(R.string.app_name));
                            builder.setMessage(getResources().getString(R.string.Yourhaveselected) +
                                    contactsSelected + getResources().getString(R.string.numberofcontactsAndweallowtosend) + jsonObj.get("message").toString()
                                    + getResources().getString(R.string.NoteSomeofyouhaveselectedcontacts))
                                    .setCancelable(false)
                                    .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                            try {
                                                //JSONArray jobjarrContsSend = jsonObj.getJSONArray("sent_contact_list");
                                                jobjarrContsSend = jsonObj.getJSONArray("sent_contact_list");
                                                //callSendSMSAsyncTask(jobjarr);
                                                dualSimManage();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
//                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            dialog.cancel();
//
//                                            if (session.getDeliveredList().size() > 0) {
//                                                callUpdateDeliveryTask();
//                                            } else {
//                                                Intent i = new Intent(SendAdsActivity.this, AdsMainActivity.class);
//                                                i.putExtra("state", Common.getAppMode());
//                                                startActivity(i);
//                                                finish();
//                                            }
//
//                                        }
//                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogAlertOK(SendAdsActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private static String[] simStatusMethodNames = {"getSimStateGemini", "getSimState"};


    private void checkDualSim(final JSONArray jobjarr) {
        boolean first = false, second = false, isDualSIM = false;

        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(SendAdsActivity.this);

        for (String methodName : simStatusMethodNames) {
            // try with sim 0 first
            try {
                first = telephonyInfo.getSIMStateBySlot(SendAdsActivity.this, methodName, 0);
                // no exception thrown, means method exists
                second = telephonyInfo.getSIMStateBySlot(SendAdsActivity.this, methodName, 1);

                String imeiSIM1 = telephonyInfo.getImsiSIM1();
                String imeiSIM2 = telephonyInfo.getImsiSIM2();

                //isDualSIM = telephonyInfo.isDualSIM();

            } catch (Exception e) {
                // method does not exist, nothing to do but test the next
            }
        }

        if (first && second) {

            new android.app.AlertDialog.Builder(SendAdsActivity.this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getResources().getString(R.string.Pleaseselectyoursim))
                    .setCancelable(false)
                    .setPositiveButton("Sim 1", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //SimUtilSMS.sendSMS(SendAdsActivity.this,0,"0775443325",null,"Sent by sim1",null,null);

                            callSendSMSAsyncTask(jobjarr, "isms");
                            dialog.dismiss();
                        }

                    })
                    .setNegativeButton("Sim 2", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //SimUtilSMS.sendSMS(SendAdsActivity.this,0,"0775443325",null,"sent by sim2",null,null);

                            callSendSMSAsyncTask(jobjarr, "isms2");
                            dialog.dismiss();
                        }
                    })
                    .show();

        } else if (first) {
            callSendSMSAsyncTask(jobjarr, "null");
        } else if (second) {
            callSendSMSAsyncTask(jobjarr, "null");
        } else {

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkDualSim(jobjarrContsSend);
                }
                break;

            default:
                break;
        }

    }

    public void dualSimManage() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            checkDualSim(jobjarrContsSend);
        }
    }
//
//
//    private void checkDualSim(){
//
//        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(SendAdsActivity.this);
//
//        String imeiSIM1 = telephonyInfo.getImsiSIM1();
//        String imeiSIM2 = telephonyInfo.getImsiSIM2();
//
//        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
//        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
//
//        boolean isDualSIM = telephonyInfo.isDualSIM();
//        Toast.makeText(SendAdsActivity.this,"imeiSIM1"+imeiSIM1+"\nimeiSIM2"+imeiSIM2+"\nisSIM1Ready"+isSIM1Ready+"\nisSIM2Ready"+isSIM2Ready+"\nisDualSIM"+isDualSIM,Toast.LENGTH_LONG).show();
//
//    }


    private void callUpdateDeliveryTask() {
        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();

            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("load_profile", Common.UPDATEPROFILE);

            ArrayList<DeliveredText> deliveredList = new ArrayList<DeliveredText>();
            //deliveredList = Common.getDeliSmsList();

            //Session session = new Session(SendAdsActivity.this);
            deliveredList.addAll(0, session.getDeliveredList());

            if (deliveredList != null && deliveredList.size() > 0) {

                JSONArray jsonArray = new JSONArray();

                for (DeliveredText delivereddetail : deliveredList) {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("ad_id", delivereddetail.getAdId());
                    jsonObj.put("msisdn", delivereddetail.getPhoneNumber());
                    jsonArray.put(jsonObj);

                }

                jsonObject.accumulate("delivered_list", jsonArray);
            }
            //convert JSONObject to JSON to String
            json = jsonObject.toString();

            new UpdateDeliveryTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Load Profile Async task class to get json by making HTTP call
     */
    class UpdateDeliveryTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(SendAdsActivity.this);
            pDialog.setMessage(getResources().getString(R.string.sendingdeliverydetails));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();
            // Making a request to url and getting response

            String strUrl = AppConfig.getBaseUrl() + urladdeliveredcontactlist;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            JSONObject jsonObj;

            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {
                        //Common.showDialogAlertOK(SendAdsActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                        //session.delDeliveredList();
                        Intent i = new Intent(SendAdsActivity.this, AdsMainActivity.class);
                        i.putExtra("state", Common.getAppMode());
                        startActivity(i);
                        finish();

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(SendAdsActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogDirecttoLogin(SendAdsActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(SendAdsActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.serverundermaintain));
            }
        }
    }

    private void showProgressDialog(String msg) {
        pDialog = new ProgressDialog(SendAdsActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage(msg);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void dismissProgressDialog() {
        if (pDialog != null || pDialog.isShowing())
            pDialog.dismiss();
    }
}


/**************************************************************************************************/
//    private void sendSmsPermission(){
//        // Here, thisActivity is the current activity
//        if (ContextCompat.checkSelfPermission(SendAdsActivity.this,
//                Manifest.permission.SEND_SMS)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale(SendAdsActivity.this,
//                    Manifest.permission.SEND_SMS)) {
//
//            } else {
//
//                ActivityCompat.requestPermissions(SendAdsActivity.this,
//                        new String[]{Manifest.permission.SEND_SMS},
//                        MY_PERMISSIONS_REQUEST_SEND_SMS);
//            }
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // contacts-related task you need to do.
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                }
//                return;
//            }
//
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
//    }

//    private void sendSMS(String phoneNumber, String message,String id) {
//        String SENT = "SMS_SENT"+phoneNumber+id;
//        String DELIVERED = "SMS_DELIVERED"+phoneNumber+id;
//
//        Intent sentIntent = new Intent(SENT);
//        sentIntent.putExtra("no", phoneNumber);
//        sentIntent.putExtra("id", id);
//
//        Intent deliveryIntent = new Intent(DELIVERED);
//        deliveryIntent.putExtra("no", phoneNumber);
//        deliveryIntent.putExtra("id", id);
//
//        //PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent( SENT), 0);
//        //PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, sentIntent, 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, deliveryIntent, 0);
//
//        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));
//        registerReceiver(deliveryBroadcastReciever, new IntentFilter(DELIVERED));
//
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
//
//    }
//
//    class DeliverReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            switch (getResultCode()) {
//                case Activity.RESULT_OK:
//                    DeliveredText deliveredText = new DeliveredText();
//                    Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
//                    String no = intent.getStringExtra("no");
//                    String id = intent.getStringExtra("id");
//                    deliveredText.setPhoneNumber(no);
//                    deliveredText.setAdId(id);
//                    deliveredTextList.add(deliveredText);
//                    Common.setDeliSmsList(deliveredTextList);
//                    break;
//                case Activity.RESULT_CANCELED:
//                    Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
//                    break;
//            }
//
//        }
//    }
//
//    class SentReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context context, Intent arg1) {
//            switch (getResultCode()) {
//                case Activity.RESULT_OK:
//                    //Toast.makeText(getBaseContext(), sms_sent, Toast.LENGTH_SHORT) .show();
//                    //startActivity(new Intent(SendSMS.this, ChooseOption.class));
//                    //overridePendingTransition(R.anim.animation, R.anim.animation2);
//                    Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                    Toast.makeText(getBaseContext(), "Generic failure",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_NO_SERVICE:
//                    Toast.makeText(getBaseContext(), "No service",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//                case SmsManager.RESULT_ERROR_NULL_PDU:
//                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
//                            .show();
//                    break;
//                case SmsManager.RESULT_ERROR_RADIO_OFF:
//                    Toast.makeText(getBaseContext(), "Radio off",
//                            Toast.LENGTH_SHORT).show();
//                    break;
//            }
//
//        }
//    }

//    /*
//         *Send sms
//         */
//    public void sendSMS(String phoneNo, String msg) {
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
//            Log.d("SMS","Message Sent");
//
//        } catch (Exception ex) {
//            Log.d("SMS","Message Sent fail");
//            ex.printStackTrace();
//        }
//    }

//    private void sendSMS(String phoneNumber, String message, String id)
//    {
//        String SENT = "SMS_SENT";
//        String DELIVERED = "SMS_DELIVERED";
//
//        Intent sentIntent = new Intent(SENT);
//        sentIntent.putExtra("no", phoneNumber);
//        sentIntent.putExtra("id", id);
//
//        Intent deliveryIntent = new Intent(DELIVERED);
//        deliveryIntent.putExtra("no", phoneNumber);
//        deliveryIntent.putExtra("id", id);
//
////        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
////        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, sentIntent, 0);
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, deliveryIntent, 0);
//
//
//        //---when the SMS has been sent---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        },new IntentFilter(SENT));
//
//        //---when the SMS has been delivered---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        DeliveredText deliveredText = new DeliveredText();
//                        Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
//                        String no = intent.getStringExtra("no");
//                        String id = intent.getStringExtra("id");
//                        deliveredText.setPhoneNumber(no);
//                        deliveredText.setAdId(id);
//                        deliveredTextList.add(deliveredText);
//                        Common.setDeliSmsList(deliveredTextList);
//
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(DELIVERED));
//
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//    }


/**************************************************/

//    private void sendSMS(String phoneNumber, String message)
//    {
//        String SENT = "SMS_SENT";
//        String DELIVERED = "SMS_DELIVERED";
//
//        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(SENT), 0);
//
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
//                new Intent(DELIVERED), 0);
//
//        //---when the SMS has been sent---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS sent",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), "Generic failure",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), "No service",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), "Null PDU",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), "Radio off",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        },new IntentFilter(SENT));
//
//        //---when the SMS has been delivered---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(getBaseContext(), "SMS not delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter(DELIVERED));
//
//        SmsManager sms = SmsManager.getDefault();
//        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//    }
//}