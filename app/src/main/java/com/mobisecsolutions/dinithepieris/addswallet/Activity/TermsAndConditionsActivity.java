package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.TermsConditionsListAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.Model.TermsCondition;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 10/11/17.
 */

public class TermsAndConditionsActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    String urlteamsandconditions ="/api/teamsandconditions";
    Session session;//global variable
    ArrayList<TermsCondition> termscondtionsList;
    TermsConditionsListAdapter termsConditionsListAdapter;
    ListView lstv_trmscon;

    public static boolean isChecked;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        getSupportActionBar().setTitle(getResources().getString(R.string.TermsAndConditions));

        init();
    }

    private void init() {

        session = new Session(TermsAndConditionsActivity.this); //in oncreate
        termscondtionsList = new  ArrayList<TermsCondition>();
        lstv_trmscon = (ListView)findViewById(R.id.lstv_trmscon);

        if(!session.getUserType().equalsIgnoreCase("null")){
            session.delUserType();
        }
        if(Common.isNetworkAvailableCheck(this)) {
            callTermsAndConditions();
        }

        Button dbtnAccept = (Button) findViewById(R.id.btn_dialog_accept);
        dbtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isChecked){
                    session.setTermsConditions("Accepted");
                    Intent i = new Intent(TermsAndConditionsActivity.this, AdsMainActivity.class);
                    i.putExtra("state",Common.APPONLINE);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(TermsAndConditionsActivity.this,getResources().getText(R.string.approve),Toast.LENGTH_LONG).show();
                }
            }
        });

        Button dbtnCancel = (Button) findViewById(R.id.btn_dialog_cancel);
        dbtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.backPressAlert(TermsAndConditionsActivity.this);
            }
        });

    }

    private void callTermsAndConditions(){
        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());

            //convert JSONObject to JSON to String
            json = jsonObject.toString();
            if(Common.isNetworkAvailableCheck(TermsAndConditionsActivity.this)) {
                new TermsAndConditionsTask().execute(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Common.backPressAlert(TermsAndConditionsActivity.this);
    }

    /**
     *  TermsAndConditions Async task class to get json by making HTTP call
     */
    private class TermsAndConditionsTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(TermsAndConditionsActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlteamsandconditions;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);

                     if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(TermsAndConditionsActivity.this, getResources().getString(R.string.app_name)
                                , jsonObj.get("message").toString());
                    } else if(jsonObj.get("state").toString().equalsIgnoreCase("1")){

                         JSONArray ads = jsonObj.getJSONArray("teams_and_conditions");

                         for (int i = 0; i < ads.length(); i++) {
                             JSONObject jobj = ads.getJSONObject(i);

                             String header = jobj.getString("heading");
                             String body = jobj.getString("body");
                             TermsCondition termsCondition = new TermsCondition();
                             termsCondition.setHeader(header);
                             termsCondition.setBody(body);
                             termscondtionsList.add(termsCondition);
                         }

                         if(termscondtionsList.size()>0) {
                             termsConditionsListAdapter = new TermsConditionsListAdapter(TermsAndConditionsActivity.this, termscondtionsList);
                             lstv_trmscon.setAdapter(termsConditionsListAdapter);
                         }
                     }
                     else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogDirecttoLogin(TermsAndConditionsActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(TermsAndConditionsActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.serverundermaintain));

            }
        }
    }
}
