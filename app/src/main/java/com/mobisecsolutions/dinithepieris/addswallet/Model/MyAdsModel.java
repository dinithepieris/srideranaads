package com.mobisecsolutions.dinithepieris.addswallet.Model;

import java.io.Serializable;

/**
 * Created by dinithepieris on 10/30/17.
 */

public class MyAdsModel implements Serializable {
    
    private String id;
    private String body;
    private String is_expired;
    private String msg_limit;
    private String msgs_per_user;
    private String current_msg_count;
    private String current_deliver_count;
    private String publish_state;
    private String published_time;
    private String expiry_time;
    private String packege_id;
    private String packege_name;
    private String packege_msg_limit;
    private String packege_msgs_per_user;
    private String packege_msg_charge;
    private String current_charge_amount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getIs_expired() {
        return is_expired;
    }

    public void setIs_expired(String is_expired) {
        this.is_expired = is_expired;
    }

    public String getMsg_limit() {
        return msg_limit;
    }

    public void setMsg_limit(String msg_limit) {
        this.msg_limit = msg_limit;
    }

    public String getMsgs_per_user() {
        return msgs_per_user;
    }

    public void setMsgs_per_user(String msgs_per_user) {
        this.msgs_per_user = msgs_per_user;
    }

    public String getCurrent_msg_count() {
        return current_msg_count;
    }

    public void setCurrent_msg_count(String current_msg_count) {
        this.current_msg_count = current_msg_count;
    }

    public String getCurrent_deliver_count() {
        return current_deliver_count;
    }

    public void setCurrent_deliver_count(String current_deliver_count) {
        this.current_deliver_count = current_deliver_count;
    }

    public String getPublish_state() {
        return publish_state;
    }

    public void setPublish_state(String publish_state) {
        this.publish_state = publish_state;
    }

    public String getPublished_time() {
        return published_time;
    }

    public void setPublished_time(String published_time) {
        this.published_time = published_time;
    }

    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
    }

    public String getPackege_id() {
        return packege_id;
    }

    public void setPackege_id(String packege_id) {
        this.packege_id = packege_id;
    }

    public String getPackege_name() {
        return packege_name;
    }

    public void setPackege_name(String packege_name) {
        this.packege_name = packege_name;
    }

    public String getPackege_msg_limit() {
        return packege_msg_limit;
    }

    public void setPackege_msg_limit(String packege_msg_limit) {
        this.packege_msg_limit = packege_msg_limit;
    }

    public String getPackege_msgs_per_user() {
        return packege_msgs_per_user;
    }

    public void setPackege_msgs_per_user(String packege_msgs_per_user) {
        this.packege_msgs_per_user = packege_msgs_per_user;
    }

    public String getPackege_msg_charge() {
        return packege_msg_charge;
    }

    public void setPackege_msg_charge(String packege_msg_charge) {
        this.packege_msg_charge = packege_msg_charge;
    }

    public String getCurrent_charge_amount() {
        return current_charge_amount;
    }

    public void setCurrent_charge_amount(String current_charge_amount) {
        this.current_charge_amount = current_charge_amount;
    }
}
