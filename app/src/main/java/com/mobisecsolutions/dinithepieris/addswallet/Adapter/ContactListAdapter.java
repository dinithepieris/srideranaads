package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Activity.SendAdsActivity;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class ContactListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ContactModel> data;
    private static LayoutInflater inflater=null;
    private ViewHolder holder;
    boolean[] checkBoxState;

    public ContactListAdapter(Context context, ArrayList<ContactModel> data) {

        this.context = context;
        this.data = data;

        inflater = LayoutInflater.from(context);

        //inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ContactModel getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //View view = convertView;
        if(convertView == null){

            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.list_row_contact, null);

            holder. name = (TextView)convertView.findViewById(R.id.txtv_name);
            holder. number = (TextView)convertView.findViewById(R.id.txtv_number);
            holder. chbxisSelected = (CheckBox)convertView.findViewById(R.id.chkbx_conSelect);
            holder. chbxisSelected.setTag( position);
            //holder.chbxisSelected.setChecked(true);

            convertView.setTag(holder);
        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

        final ContactModel contactModel = data.get(position);

        // Setting all values in listview
        if(contactModel!=null) {
            holder.name.setText(contactModel.getName());
            holder.number.setText(contactModel.getMobileNumber());
            holder.chbxisSelected.setChecked(contactModel.getisSelected());
            //holder.chbxisSelected.setTag(R.integer.btnplusview, convertView);
        }



        holder.chbxisSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                //holder.chbxisSelected.setChecked(false);
                contactModel.setisSelected(checked);
                if(checked){
                    //SendAdsActivity.txtv_selecnumber.setText("Selected: "+ SendAdsActivity.ADS_selcted_cnt++);
                }else{
                    //SendAdsActivity.txtv_selecnumber.setText("Selected: "+ SendAdsActivity.ADS_selcted_cnt--);
                    SendAdsActivity.chkbx_selectAll.setChecked(false);
                }

            }
        });



        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView name;
        TextView number;
        CheckBox chbxisSelected;

    }

//    public void selectAll() {
//        for(int i=0; i>data.size(); i++){
//            data.get(i).setisSelected(true);
//        }
//        notifyDataSetChanged();
//    }
//
//    public void unselectAll() {
//        for(int i=0; i>data.size(); i++){
//            data.get(i).setisSelected(false);
//        }
//        notifyDataSetChanged();
//    }

}
