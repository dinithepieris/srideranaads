package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Activity.TermsAndConditionsActivity;
import com.mobisecsolutions.dinithepieris.addswallet.Model.PromoAdInfo;
import com.mobisecsolutions.dinithepieris.addswallet.Model.TermsCondition;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class TermsConditionsListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<TermsCondition> data;
    private static LayoutInflater inflater=null;


    public TermsConditionsListAdapter(Context context, ArrayList<TermsCondition> data) {
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(convertView == null)
            view = inflater.inflate(R.layout.list_row_terms_conditions, null);

        TextView title = (TextView)view.findViewById(R.id.txtv_header);
        TextView body = (TextView)view.findViewById(R.id.txtv_body);
        CheckBox chbx_termsconditions = (CheckBox)view.findViewById(R.id.chbx_termsconditions);


        TermsCondition trmobj = data.get(position);

        title.setText(trmobj.getHeader());
        body.setText(trmobj.getBody());
        chbx_termsconditions.setVisibility(View.GONE);

        if(position +1== data.size()){
            chbx_termsconditions.setVisibility(View.VISIBLE);
            chbx_termsconditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    TermsAndConditionsActivity.isChecked = checked;
                }
            });
        }

        return view;
    }
}
