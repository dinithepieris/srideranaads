package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.support.v7.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactMessageModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 10/12/17.
 */

public class RecyclerMessageAdapter extends RecyclerView.Adapter<RecyclerMessageAdapter.ViewHolder> {

    ArrayList <ContactMessageModel> data;
    Context context;
    ViewHolder msgViewHolder;

    public RecyclerMessageAdapter(Context context, ArrayList <ContactMessageModel> data) {
        this.context = context;
        this.data = data;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // infalte the item Layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_contactus, parent, false);
        // set the view's size, margins, paddings and layout parameters
        msgViewHolder = new ViewHolder(v); // pass the view to View Holder
        return msgViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerMessageAdapter.ViewHolder holder, int position) {

        //ContactMessageModel contactMessageModel = data.get(position)

        // set the data in items
        holder.txtv_message.setText(data.get(position).getMessage());
        holder.txtv_message_time.setText(data.get(position).getMessage_time());
        if(data.get(position).getState().equalsIgnoreCase("1")){
            holder.txtv_reply.setText(data.get(position).getReply());
            holder.txtv_reply_time.setText(data.get(position).getReply_time());
        }else{
            holder.ll_chatans.setVisibility(View.GONE);
        }

        //holder.setIsRecyclable(false);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

//    public void add(int position, String item) {
//        data.add(position, item);
//        notifyItemInserted(position);
//    }
//
//    public void remove(int position) {
//        data.remove(position);
//        notifyItemRemoved(position);
//    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // init the item view's
        TextView txtv_message, txtv_message_time, txtv_reply, txtv_reply_time;
        LinearLayout ll_chatquz, ll_chatans;

        public ViewHolder(View itemView) {
            super(itemView);
            // get the reference of item view's
            txtv_message = (TextView) itemView.findViewById(R.id.txtv_message);
            txtv_message_time = (TextView) itemView.findViewById(R.id.txtv_message_time);
            txtv_reply = (TextView) itemView.findViewById(R.id.txtv_reply);
            txtv_reply_time = (TextView) itemView.findViewById(R.id.txtv_reply_time);

            ll_chatquz = (LinearLayout) itemView.findViewById(R.id.ll_chatquz);
            ll_chatans = (LinearLayout) itemView.findViewById(R.id.ll_chatans);

        }
    }

//    @Override
//    public void onViewAttachedToWindow(final RecyclerMessageAdapter.ViewHolder holder) {
//        if (holder instanceof ViewHolder) {
//            holder.setIsRecyclable(false);
//        }
//        super.onViewAttachedToWindow(holder);
//    }
//
//    @Override
//    public void onViewDetachedFromWindow(final RecyclerMessageAdapter.ViewHolder holder) {
//        if (holder instanceof ViewHolder){
//            holder.setIsRecyclable(true);
//        }
//        super.onViewDetachedFromWindow(holder);
//    }

    //    @Override
//    public void onViewAttachedToWindow(final RecyclerView.ViewHolder holder) {
//        if (holder instanceof VideoViewHolder) {
//            holder.setIsRecyclable(false);
//        }
//        super.onViewAttachedToWindow(holder);
//    }
//
//    @Override
//    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder holder) {
//        if (holder instanceof VideoViewHolder){
//            holder.setIsRecyclable(true);
//        }
//        super.onViewDetachedFromWindow(holder);
//    }
}
