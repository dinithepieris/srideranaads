package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Model.TermsCondition;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;

//                Session session;//global variable
//                session = new Session(LoginActivity.this); //in oncreate
//                //and now we set sharedpreference then use this like
//                session.setusename(edt_usname.getText().toString());

/**
 * Created by Dinithe Pieris on 8/1/2017.
 */

public class LoginActivity extends AppCompatActivity {

    EditText edt_usname,edt_psword;
    Button btn_login, btn_newuser;
    String uname ,pwrd;

    ProgressDialog pDialog;
    String urlLogin = "/api/signin";

    Session session;//global variable


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setTitle(getResources().getString(R.string.login));
        session = new Session(LoginActivity.this); //in oncreate

        init();

    }

    private void init() {

        edt_usname = (EditText)findViewById(R.id.edt_usname);
        edt_psword = (EditText)findViewById(R.id.edt_psword);


        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(isValidated()){
                    try {
                        String json = "";
                        // build jsonObject
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.accumulate("msisdn", uname);
                        jsonObject.accumulate("device_id", Common.getAndroidDeviceId(getApplicationContext()));
                        jsonObject.accumulate("password", pwrd);

                        //convert JSONObject to JSON to String
                        json = jsonObject.toString();
                        if(Common.isNetworkAvailableCheck(LoginActivity.this)) {
                            new UserLoginTask().execute(json);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        btn_newuser = (Button)findViewById(R.id.btn_newuser);
        btn_newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginIntent = new Intent(LoginActivity.this, UserRegistrationActivity.class);
                LoginActivity.this.startActivity(loginIntent);
                LoginActivity.this.finish();

            }
        });
    }

    private boolean isValidated() {

         uname = edt_usname.getText().toString().trim();
         pwrd = edt_psword.getText().toString().trim();

        if (uname.length() != 10) {
            Toast.makeText(this, "Invalid Mobile Number", Toast.LENGTH_LONG).show();
            return false;
        }

        if (pwrd.length() < 6) {
            Toast.makeText(this, "Invalid Password!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Common.backPressAlert(this);
    }
    /**
     * Login Async task class to get json by making HTTP call
     */
    private class UserLoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlLogin;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject  jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){
                        Common.setToken(jsonObj.getString("token"));

                         //and now we set sharedpreference then use this like
                        session.setToken(Common.getToken());

//                        Intent nextIntent = new Intent(LoginActivity.this,AdsMainActivity.class);
//                        nextIntent.putExtra("state", Common.APPONLINE);
//                        startActivity(nextIntent);
//                        finish();

                        if(session.getTermsConditions().equalsIgnoreCase("null")){

                            Intent nextIntent = new Intent(LoginActivity.this,TermsAndConditionsActivity.class);
                            startActivity(nextIntent);
                            finish();

                        }else if((session.getTermsConditions().equalsIgnoreCase("Accepted"))
                                ||(session.getTermsConditions().equalsIgnoreCase("Accepted_synced"))){
                            Intent nextIntent = new Intent(LoginActivity.this,AdsMainActivity.class);
                            nextIntent.putExtra("state", Common.APPONLINE);
                            startActivity(nextIntent);
                            finish();
                        }

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        edt_psword.setText("");
                        Common.showDialogAlertOK(LoginActivity.this, getResources().getString(R.string.app_name) , jsonObj.get("message").toString());

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {
                        edt_psword.setText("");
                        edt_usname.setText("");
                        Common.showDialogAlertOK(LoginActivity.this, getResources().getString(R.string.app_name) , jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(LoginActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
                edt_psword.setText("");

            }
        }
    }
}
