package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/22/17.
 */

public class SplashActivity extends Activity {

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000*1;
    Session session;
    boolean sessionVerifi = false;
    boolean isNetConnected = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        session = new Session(SplashActivity.this); //in oncreate



//        if(session.getUserType().equalsIgnoreCase("null")){
//
//            usertypeSelection();
//
//        } else if(session.getUserType().equalsIgnoreCase(Common.PUBLISHER)){

            isSessionVerifi();

            isInternetConneted();

            showContacts();
//
//        } else if(session.getUserType().equalsIgnoreCase(Common.ADVERTISER)){
//            Intent i = new Intent(SplashActivity.this, AdvertiserLoginActivity.class);
//            startActivity(i);
//
//        }
        //longinNavigation();

    }


    /**
     * Show the contacts in the ListView.
     */
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            new LoadContactTask().execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, getResources().getText(R.string.contactpermission), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     */
    class LoadContactTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            loadContactList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if(sessionVerifi && isNetConnected) {

                if(session.getTermsConditions().equalsIgnoreCase("null")){
                    Intent i = new Intent(SplashActivity.this, TermsAndConditionsActivity.class);
                    startActivity(i);
                    finish();
                }
                else{
                    Intent i = new Intent(SplashActivity.this, AdsMainActivity.class);
                    i.putExtra("state",Common.APPONLINE);
                    startActivity(i);
                    finish();
                }
            } else if (isNetConnected) {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            } else {
                //nextDialog("Internet Error!","Please connect to the internet!");
                Common.isNetworkAvailableCheck(SplashActivity.this);
            }
        }
    }

    private void longinNavigation(){

        new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    private void loadContactList(){

        ArrayList<ContactModel> contactModelList = new ArrayList<ContactModel>();
        contactModelList = Common.getDeviceContacts(this);

        Common.setMyContacts(contactModelList);
//        if(session.getConLstCnt()==0) {
//            session.setConLstCnt(contactModelList.size());
//        }
    }

    private void isSessionVerifi(){
        //if(session.getToken().length()>0){
        if(!session.getToken().equalsIgnoreCase("null")){
            sessionVerifi = true;
            Common.setToken(session.getToken());
        }
    }

    private void isInternetConneted(){
        if (Common.isNetworkAvailable(SplashActivity.this)) {
            isNetConnected = true;
        }
    }

    private void nextDialog(String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getText(R.string.Ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }

    private void usertypeSelection(){

        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_select_mytype);
        dialog.setTitle("Select My type:");

        Button dbtnPublisher = (Button) dialog.findViewById(R.id.btn_dialog_publisher);

        dbtnPublisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setUserType(Common.PUBLISHER);
                isSessionVerifi();
                isInternetConneted();
                showContacts();
                dialog.dismiss();
            }
        });


        Button dbtnAdvertiser = (Button) dialog.findViewById(R.id.btn_dialog_advertiser);

        dbtnAdvertiser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setUserType(Common.ADVERTISER);
                Intent i = new Intent(SplashActivity.this, AdvertiserLoginActivity.class);
                startActivity(i);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

}
