package com.mobisecsolutions.dinithepieris.addswallet.Common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobisecsolutions.dinithepieris.addswallet.Model.DeliveredText;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinithe Pieris on 8/1/2017.
 */

public class Session {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;


    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        editor = prefs.edit();
    }
    //Token
    public void setToken(String token) {
        prefs.edit().putString("token", token).commit();
        //prefsCommit();
    }

    public String getToken() {
        String token = prefs.getString("token","null");
        return token;
    }

    public void delToken() {
        prefs.edit().remove("token").commit();
    }

    //last Ad
    public void setAdId(String adId) {
        prefs.edit().putString("adId", adId).commit();
        //prefsCommit();
    }

    public String getAdId() {
        String adId = prefs.getString("adId","null");
        return adId;
    }

    public void delAdId() {
        prefs.edit().remove("adId").commit();
    }

    //contact list cnt
    public void setConLstCnt(int conLstCnt) {
        prefs.edit().putInt("ConLstCnt", conLstCnt).commit();
        //prefsCommit();
    }

    public int getConLstCnt() {
        int conLstCnt = prefs.getInt("ConLstCnt", 0);
        return conLstCnt;
    }

    public void delConLstCnt() {
        prefs.edit().remove("ConLstCnt").commit();
    }

    //UserType
    public void setUserType(String uType) {
        prefs.edit().putString("UserType", uType).commit();
        //prefsCommit();
    }

    public String getUserType() {
        String uType = prefs.getString("UserType","null");
        return uType;
    }

    public void delUserType() {
        prefs.edit().remove("UserType").commit();
    }

    //Delivered list
    public void setDeliveredList(ArrayList<DeliveredText> dList) {
        //prefs.edit().putString("dList", dList).commit();
        Gson gson = new Gson();
        String json_dList = gson.toJson(dList);
        prefs.edit().putString("dList", json_dList).commit();
        //prefsCommit();
    }

    public ArrayList<DeliveredText> getDeliveredList() {
        //String dList = prefs.getString("dList","null");
        //return dList;
        ArrayList<DeliveredText> dList = new ArrayList<DeliveredText>();
        Gson gson = new Gson();
        String json = prefs.getString("dList", "");
        if (json.isEmpty()) {
            dList = new ArrayList<DeliveredText>();
        } else {
            Type type = new TypeToken<List<DeliveredText>>() {
            }.getType();
            dList = gson.fromJson(json, type);
        }
        return dList;
    }

    public void delDeliveredList() {
        prefs.edit().remove("dList").commit();
    }


    //terms and conditions
    public void setTermsConditions(String termsconditions) {
        prefs.edit().putString("termsconditions", termsconditions).commit();
        //prefsCommit();
    }

    public String getTermsConditions() {
        String termsconditions = prefs.getString("termsconditions","null");
        return termsconditions;
    }

    public void delTermsConditions() {
        prefs.edit().remove("termsconditions").commit();
    }


    public void setConTimeOut(int conTimeOut) {
        prefs.edit().putInt("ConTimeOut", conTimeOut).commit();
        //prefsCommit();
    }

    public int getConTimeOut() {
        int conLstCnt = prefs.getInt("TimeOut", 0);
        return conLstCnt;
    }

    public void delConTimeOut() {
        prefs.edit().remove("TimeOut").commit();
    }


}
