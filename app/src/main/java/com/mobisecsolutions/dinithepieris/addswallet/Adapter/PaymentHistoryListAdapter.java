package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.PaymentHistoryModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class PaymentHistoryListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PaymentHistoryModel> data;
    private static LayoutInflater inflater=null;
    private ViewHolder holder = null;

    public PaymentHistoryListAdapter(Context context, ArrayList<PaymentHistoryModel> data) {
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if(view == null){

            holder = new ViewHolder();

            view = inflater.inflate(R.layout.list_row_payment_history, null);

            holder. txtv_payment = (TextView)view.findViewById(R.id.txtv_payment);
            holder. txtv_paystate = (TextView)view.findViewById(R.id.txtv_paystate);
            holder. txtv_paytype = (TextView)view.findViewById(R.id.txtv_paytype);
            holder. txtv_paynumber = (TextView)view.findViewById(R.id.txtv_paynumber);
            holder. txtv_payreqdate = (TextView)view.findViewById(R.id.txtv_payreqdate);
            holder. txtv_payrcddate = (TextView)view.findViewById(R.id.txtv_payrcddate);
            holder. txtv_paybranch = (TextView)view.findViewById(R.id.txtv_paybranch);
            holder. txtv_payaccname = (TextView)view.findViewById(R.id.txtv_payaccname);
            holder. txtv_payaccnumber = (TextView)view.findViewById(R.id.txtv_payaccnumber);

            holder. llay_bankdetails = (LinearLayout)view.findViewById(R.id.llay_bankdetails);

            view.setTag(holder);
        }
        else{

            holder = (ViewHolder)convertView.getTag();
        }

        final PaymentHistoryModel model = data.get(position);

        if(model!=null) {
            holder.txtv_payment.setText("Rs." + model.getAmount());
            holder.txtv_paystate.setText(getState(Integer.parseInt(model.getState().trim())));
            holder.txtv_paytype.setText(getType(Integer.parseInt(model.getPayment_type_id().trim())));
            if(Integer.parseInt(model.getPayment_type_id().trim())==4){
                holder.txtv_paynumber.setText("Bank: " + model.getBank_name());
            }else {
                holder.txtv_paynumber.setText("Contact Number: " + model.getRequest_msisdn());
            }
            holder.txtv_payreqdate.setText("Requested on: "+model.getAdded_time());
            holder.txtv_payrcddate.setText("Recived on: "+model.getPaid_time());
            holder.txtv_paybranch.setText("Branch: "+model.getBranch_name());
            holder.txtv_payaccname.setText("Account name: "+model.getAcc_name());
            holder.txtv_payaccnumber.setText("Account number: "+model.getAcc_number());
        }
        return view;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView txtv_payment, txtv_paystate, txtv_paytype, txtv_paynumber,
        txtv_payreqdate, txtv_payrcddate, txtv_paybranch, txtv_payaccname, txtv_payaccnumber;
        LinearLayout llay_bankdetails;

    }

    private String getType(int i){

        switch (i) {
            case 1:

                holder.llay_bankdetails.setVisibility(View.GONE);
                return "Eazy Cash";


            case 2:

                holder.llay_bankdetails.setVisibility(View.GONE);
                return "M Cash";

            case 3:

                holder.llay_bankdetails.setVisibility(View.GONE);
                return "Reload";

            case 4:

                holder.llay_bankdetails.setVisibility(View.VISIBLE);
                return "Bank";


        }
        return null;
    }

    private String getState(int i){

        switch (i) {
            case 0:
                holder. txtv_paystate.setTextColor(Color.MAGENTA);
                holder.txtv_payrcddate.setVisibility(View.GONE);
                return "Pending";
            case 1:
                holder. txtv_paystate.setTextColor(Color.GREEN);
                return "Paid";

        }
        return null;
    }
}
