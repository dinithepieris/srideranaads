package com.mobisecsolutions.dinithepieris.addswallet.Model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class PaymentHistoryModel {

    public String id;
    public String amount;
    public String appuser_id;
    public String payment_type_id;
    public String request_msisdn;
    public String bank_details_id;
    public String bank_name;
    public String branch_name;
    public String acc_name;
    public String acc_number;
    public String state;
    public String paid_by;
    public String added_time;
    public String paid_time;
    public String can_see;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAppuser_id() {
        return appuser_id;
    }

    public void setAppuser_id(String appuser_id) {
        this.appuser_id = appuser_id;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getRequest_msisdn() {
        return request_msisdn;
    }

    public void setRequest_msisdn(String request_msisdn) {
        this.request_msisdn = request_msisdn;
    }

    public String getBank_details_id() {
        return bank_details_id;
    }

    public void setBank_details_id(String bank_details_id) {
        this.bank_details_id = bank_details_id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getAcc_name() {
        return acc_name;
    }

    public void setAcc_name(String acc_name) {
        this.acc_name = acc_name;
    }

    public String getAcc_number() {
        return acc_number;
    }

    public void setAcc_number(String acc_number) {
        this.acc_number = acc_number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPaid_by() {
        return paid_by;
    }

    public void setPaid_by(String paid_by) {
        this.paid_by = paid_by;
    }

    public String getAdded_time() {
        return added_time;
    }

    public void setAdded_time(String added_time) {
        this.added_time = added_time;
    }

    public String getPaid_time() {
        return paid_time;
    }

    public void setPaid_time(String paid_time) {
        this.paid_time = paid_time;
    }

    public String getCan_see() {
        return can_see;
    }

    public void setCan_see(String can_see) {
        this.can_see = can_see;
    }
}
