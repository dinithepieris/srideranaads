package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.AdsPackageAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Model.AdsPackageModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 10/25/17.
 */

public class AdvertiseActivity extends AppCompatActivity {

    EditText edt_message, edt_msgcount, edt_msgperperson, edt_msgpubdate, edt_msgexpiredate,
            edt_msgpaymentamount, edt_msgcontact, edt_district;
    Button btn_pay, btn_back;


    ProgressDialog pDialog;
    String urlgetpackages = "/api/getpackages";
    String urladpublish = "/api/adpublish";
    JSONObject jsonObj;
    ArrayList<AdsPackageModel> adsPackage;

    int LOAD = 1;
    int ADVERTISE = 2;
    String SELECTED_PACKAGE;

    String totalearn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise);
        getSupportActionBar().setTitle(getResources().getString(R.string.postnewads));

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                totalearn = null;
            } else {
                totalearn = extras.getString("totalearn");
            }
        } else {
            totalearn = (String) savedInstanceState.getSerializable("totalearn");
        }

        init();
        callLoadingTask();

    }

    private void init() {

        edt_message = (EditText) findViewById(R.id.edt_message);

        edt_msgcount = (EditText) findViewById(R.id.edt_msgcount);
        edt_msgcount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();

            }
        });

        edt_msgperperson = (EditText) findViewById(R.id.edt_msgperperson);
        edt_msgperperson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        edt_msgpubdate = (EditText) findViewById(R.id.edt_msgpubdate);
        edt_msgpubdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog(edt_msgpubdate);
            }
        });
        edt_msgexpiredate = (EditText) findViewById(R.id.edt_sgexpiredate);
        edt_msgexpiredate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog(edt_msgexpiredate);
            }
        });

        edt_district = (EditText)findViewById(R.id.edt_district);
        edt_district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] arrProvince = getResources().getStringArray(R.array.province);

                List<String> list = new ArrayList<String>();
                list = Arrays.asList(arrProvince);
                ArrayList<String> arrayList = new ArrayList<String>(list);
                arrayList.add(0,"All Districts");
                arrProvince = arrayList.toArray(new String[list.size()]);

                Common.listviewSelector(AdvertiseActivity.this,arrProvince,edt_district);
            }
        });

        edt_msgpaymentamount = (EditText) findViewById(R.id.edt_msgpaymentamount);
        edt_msgcontact = (EditText) findViewById(R.id.edt_msgcontact);

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_pay = (Button) findViewById(R.id.btn_pay);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AdvertiseActivity.this, AdsMainActivity.class);
                i.putExtra("state", Common.APPONLINE);
                startActivity(i);
                finish();
            }
        });

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatedata()) {

                    Double payval = 0.0;
                    Double earnval = Double.parseDouble(totalearn.trim());

                    if (edt_msgpaymentamount.getText().toString().trim().length() > 0) {
                        payval = Double.parseDouble(edt_msgpaymentamount.getText().toString().trim().split(":")[1]);
                    }
                    else{
                        Common.showDialogDirect(AdvertiseActivity.this,
                                getResources().getString(R.string.app_name),getResources().getString(R.string.Youraccountbalanceisinsufficient), "AddCreditActivity");
                    }

                    if (earnval > payval) {
                        callPostAdTask();
                    } else {
                        Common.showDialogDirect(AdvertiseActivity.this,
                                getResources().getString(R.string.app_name),getResources().getString(R.string.Youraccountbalanceisinsufficient), "AddCreditActivity");
                    }

                }
            }
        });
    }

    private void showDateDialog(final EditText editText) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setLenient(false);

        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editText.setText(dateFormatter.format(newDate.getTime()));
                showTimeDialog(editText);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void showTimeDialog(final EditText editText) {
        Calendar newCalendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
                editText.setText(editText.getText().toString() + " " + hourOfDay + ":" + minute + ":00");
            }

        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();
    }


    private void callLoadingTask() {

        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());

            //convert JSONObject to JSON to String
            json = jsonObject.toString();
            if (Common.isNetworkAvailableCheck(AdvertiseActivity.this)) {
                new AdvertiseLoaderTask(LOAD, urlgetpackages).execute(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void callPostAdTask() {

        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("body", edt_message.getText().toString().trim());
            jsonObject.accumulate("package_id", SELECTED_PACKAGE);
            jsonObject.accumulate("published_time", edt_msgpubdate.getText().toString().trim());
            jsonObject.accumulate("expiry_time", edt_msgexpiredate.getText().toString().trim());
            jsonObject.accumulate("district", edt_district.getText().toString().trim());
            jsonObject.accumulate("msisdn", edt_msgcontact.getText().toString().trim());

            //convert JSONObject to JSON to String
            json = jsonObject.toString();
            if (Common.isNetworkAvailableCheck(AdvertiseActivity.this)) {
                new AdvertiseLoaderTask(ADVERTISE, urladpublish).execute(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(AdvertiseActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    private boolean validatedata() {
        Date expiredate = null, publishdate = null, currentdate = null;

        if (edt_message.getText().toString().trim().length() < 1) {
            Common.validationDialog(this, getResources().getString(R.string.PleaseentervalidMessageforpublish));
            return false;
        }
        if (edt_msgcount.getText().toString().trim().length() < 1) {
            Common.validationDialog(this, getResources().getString(R.string.PleasesetMessagecount));
            return false;
        }
        if (edt_msgpubdate.getText().toString().trim().length() < 1) {
            Common.validationDialog(this, getResources().getString(R.string.PleaseSelectpublishdateandtime));
            return false;
        }
        if (edt_msgexpiredate.getText().toString().trim().length() < 1) {
            Common.validationDialog(this, getResources().getString(R.string.Pleaseselectexpiredateandtime));
            return false;
        }

        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            publishdate = dateFormatter.parse(edt_msgpubdate.getText().toString());
            expiredate = dateFormatter.parse(edt_msgexpiredate.getText().toString());
            currentdate = dateFormatter.parse(Common.getDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (currentdate.after(publishdate)) {//||(!currentDate.equals(publishdate)))){
            Common.validationDialog(this, getResources().getString(R.string.Pleaseentervaliddateforpublishdate));
            return false;

        } else if (currentdate.after(expiredate)) { //||(!currentDate.equals(expiredate)))){
            Common.validationDialog(this, getResources().getString(R.string.Pleaseentervaliddateforexpiredate));
            return false;

        } else if (publishdate.after(expiredate)) { //||(!publishdate.equals(expiredate))){
            Common.validationDialog(this,  getResources().getString(R.string.Expiredateshouldbeafterthepublishdate));
            return false;
        }

        if(edt_district.getText().toString().trim().length()< 3){
            Common.validationDialog(this,getResources().getString(R.string.PleaseSelectProvince));
            return false;
        }

        if ((edt_msgcontact.getText().toString().trim().length() != 10)
                || (edt_msgcontact.getText().toString().trim().charAt(0) != '0')
                || (edt_msgcontact.getText().toString().trim().charAt(1) != '7')) {
            Common.validationDialog(this, getResources().getString(R.string.pleaseentervalidcontactnumber));
            return false;
        }


        return true;
    }

//    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//    @Override
//    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
//        super.onActivityResult(reqCode, resultCode, data);
//
//        if (reqCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//            Bitmap image = (Bitmap) data.getExtras().get("data");
//            //imageView.setImageBitmap(photo);
//            IMAGESTR = getEncoded64ImageStringFromBitmap(image);
////            BitmapDrawable bdrawable = new BitmapDrawable(this.getResources(),image);
////            btn_upload.setBackground(bdrawable);
//            btn_upload.setText("Image successfully uploaded");
//        } else if (reqCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
//            try {
//                final Uri imageUri = data.getData();
//                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
//                final Bitmap image = BitmapFactory.decodeStream(imageStream);
//                IMAGESTR = getEncoded64ImageStringFromBitmap(image);
//                //image_view.setImageBitmap(image);
//
////                BitmapDrawable bdrawable = new BitmapDrawable(this.getResources(),image);
////                btn_upload.setBackground(bdrawable);
//                btn_upload.setText("Image successfully uploaded");
//
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//                Toast.makeText(AdvertiseActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
//            }
//
//        } else {
//            Toast.makeText(AdvertiseActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    //String encodedImageData =getEncoded64ImageStringFromBitmap(your bitmap);
//
//    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
//        byte[] byteFormat = stream.toByteArray();
//        // get the base 64 string
//        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
//
//        return imgString;
//    }
//
//    public void showDialogSelectInput() {
//        AlertDialog alertDialog = new AlertDialog.Builder(AdvertiseActivity.this).create();
//        alertDialog.setTitle(getApplicationContext().getResources().getString(R.string.app_name));
//        alertDialog.setMessage("Please import your bank slip : ");
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Capture Image",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
//
//                        dialog.dismiss();
//
//                    }
//                });
//        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Gallery Image",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                        photoPickerIntent.setType("image/*");
//                        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
//
//                        dialog.dismiss();
//
//                    }
//                });
//        alertDialog.show();
//        alertDialog.setCancelable(false);
//    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class AdvertiseLoaderTask extends AsyncTask<String, Void, String> {

        String url;
        int type;

        private AdvertiseLoaderTask(int type, String url) {
            this.type = type;
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AdvertiseActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Loading));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + url;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null && type == LOAD) {
                try {
                    jsonObj = new JSONObject(result);
                    if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(AdvertiseActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {
                        JSONArray ads = jsonObj.getJSONArray("packeges");
                        adsPackage = new ArrayList<AdsPackageModel>();

                        //looping through All Contacts
                        for (int i = 0; i < ads.length(); i++) {
                            JSONObject c = ads.getJSONObject(i);

                            String id = c.getString("id");
                            String name = c.getString("name");
                            //String amount = c.getString("amount");
                            String msg_limit = c.getString("msg_limit");
                            String msgs_per_user = c.getString("msgs_per_user");
                            String msg_charge = c.getString("msg_charge");
                            String state = c.getString("state");
                            String added_time = c.getString("added_time");
                            String user_id = c.getString("user_id");

                            AdsPackageModel pkgInfo = new AdsPackageModel();

                            pkgInfo.setId(id);
                            pkgInfo.setName(name);
                            pkgInfo.setAmount("" + Double.parseDouble(msg_limit) * Double.parseDouble((msg_charge)));
                            pkgInfo.setMsg_limit(msg_limit);
                            pkgInfo.setMsgs_per_user(msgs_per_user);
                            pkgInfo.setMsg_charge(msg_charge);
                            pkgInfo.setAdded_time(added_time);
                            pkgInfo.setUser_id(user_id);

                            if (state.equalsIgnoreCase("1")) {
                                adsPackage.add(pkgInfo);
                            }
                        }

                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else if (result != null && type == ADVERTISE) {
                try {
                    jsonObj = new JSONObject(result);
                    if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(AdvertiseActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {
                        Common.showDialogDirect(AdvertiseActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString(), "AdsMainActivity");
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {
                        Common.showDialogAlertOK(AdvertiseActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(AdvertiseActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.serverundermaintain));
            }
        }
    }

    private void showDialog() {

        final Dialog dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_adspackage, null);
        ListView lv = (ListView) view.findViewById(R.id.lstv_adspackage);
        AdsPackageAdapter adapter = new AdsPackageAdapter(AdvertiseActivity.this, adsPackage);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                edt_msgcount.setText(getResources().getText(R.string.SMSCount) + adsPackage.get(position).getMsg_limit());
                edt_msgperperson.setText(getResources().getText(R.string.SMSperuser) + adsPackage.get(position).getMsgs_per_user());
                edt_msgpaymentamount.setText(getResources().getText(R.string.AmountRs) + adsPackage.get(position).getAmount());
                SELECTED_PACKAGE = adsPackage.get(position).getId();
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.show();

    }
}