package com.mobisecsolutions.dinithepieris.addswallet.Model;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class PromoAdInfo {
    String id, body, user_id, added_time, payment, state, expiry_date, is_expired, msg_limit, current_msg_count ,msgs_per_user, deliver_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

//    public String getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(String user_id) {
//        this.user_id = user_id;
//    }

    public String getAdded_time() {
        return added_time;
    }

    public void setAdded_time(String added_time) {
        this.added_time = added_time;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getIs_expired() {
        return is_expired;
    }

    public void setIs_expired(String is_expired) {
        this.is_expired = is_expired;
    }

    public String getMsg_limit() {
        return msg_limit;
    }

    public void setMsg_limit(String msg_limit) {
        this.msg_limit = msg_limit;
    }

    public String getCurrent_msg_count() {
        return current_msg_count;
    }

    public void setCurrent_msg_count(String current_msg_count) {
        this.current_msg_count = current_msg_count;
    }

    public String getMsgs_per_user() {
        return msgs_per_user;
    }

    public void setMsgs_per_user(String msgs_per_user) {
        this.msgs_per_user = msgs_per_user;
    }

    public String getDeliver_count() {
        return deliver_count;
    }

    public void setDeliver_count(String deliver_count) {
        this.deliver_count = deliver_count;
    }
}
