package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 9/26/17.
 */

public class ChangePasswordActivity extends AppCompatActivity {

    EditText edt_existingpassword, edt_newpassword, edt_newpasswordConform;
    Button btn_cancel, btn_conform;
    ProgressDialog pDialog;
    String urlLogin = "/api/resetpassword";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setTitle(getResources().getString(R.string.changepassword));
        init();
    }

    private void init() {

        edt_existingpassword = (EditText) findViewById(R.id.edt_existingpassword);
        edt_newpassword = (EditText) findViewById(R.id.edt_newpassword);
        edt_newpasswordConform = (EditText) findViewById(R.id.edt_newpasswordConform);

        btn_conform = (Button) findViewById(R.id.btn_conform);
        btn_cancel = (Button) findViewById(R.id.btn_concancel);

        btn_conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validatePasswordResetData()&&Common.isNetworkAvailableCheck(ChangePasswordActivity.this)){
                    callResetPasswordTask();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChangePasswordActivity.this, AdsMainActivity.class);
                i.putExtra("state", Common.APPONLINE);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ChangePasswordActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    private boolean validatePasswordResetData(){

        if(edt_existingpassword.getText().toString().trim().length()< 5){
            Common.validationDialog(this,"Please enter existing password!");
            return false;
        }
        if(edt_newpassword.getText().toString().trim().length()< 6){
            Common.validationDialog(this,"Please enter more than 6 letter password!");
            return false;
        }

        if(!edt_newpasswordConform.getText().toString().trim().
                equals(edt_newpasswordConform.getText().toString().trim())){
            Common.validationDialog(this,"Passwords mismached!");
            return false;
        }

        return true;
    }

    private void callResetPasswordTask(){

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("existing_password", edt_existingpassword.getText().toString().trim());
            jsonObject.accumulate("new_password", edt_newpassword.getText().toString().trim());

            String json = jsonObject.toString();
            new PasswordResetTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class PasswordResetTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ChangePasswordActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlLogin;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){
                        Common.showDialogDirecttoLogin(ChangePasswordActivity.this,  getResources().getString(R.string.app_name),jsonObj.getString("message"));

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(ChangePasswordActivity.this, getResources()
                                .getString(R.string.app_name), jsonObj.getString("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(ChangePasswordActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.serverundermaintain));
            }
        }
    }
}
