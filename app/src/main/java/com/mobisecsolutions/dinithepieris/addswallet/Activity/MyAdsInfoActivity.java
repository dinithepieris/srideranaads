package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.MyAdsModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

/**
 * Created by dinithepieris on 10/31/17.
 */

public class MyAdsInfoActivity extends AppCompatActivity {

    TextView txtv_status, txtv_package, txtv_msg,txtv_msgcnt,txtv_publisheddate,txtv_expiredate,
                txtv_msgperuser,txtv_msgsent, txtv_msgdelivered, txtv_payment, txtv_paymentType;
    MyAdsModel myads;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myadsinfo);
        getSupportActionBar().setTitle(getResources().getString(R.string.myadsinfo));

        myads = new MyAdsModel();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //test
            } else {
                myads = (MyAdsModel) getIntent().getSerializableExtra("strjson");

            }
        } else {
            myads = (MyAdsModel) savedInstanceState.getSerializable("strjson");
        }
        init();
    }

    private void init() {

        String publishState = null;
        if(!Boolean.valueOf(myads.getIs_expired())){
            switch(Integer.valueOf(myads.getPublish_state())) {
                case 1 :
                    publishState = "Pending";
                    break; // optional
                case 2 :
                    publishState = "Published";
                    break; // optional
                case 3 :
                    publishState = "Rejected";
                    break; // optional
            }
        }else{
            publishState = "Expired!";
        }

        txtv_status = (TextView)findViewById(R.id.txtv_status);
        txtv_status.setText(getResources().getText(R.string.Adstate) +publishState);

        txtv_package = (TextView)findViewById(R.id.txtv_package);
        txtv_package.setText(getResources().getText(R.string.SelectedPackage)+myads.getPackege_name());

        txtv_msg = (TextView)findViewById(R.id.txtv_msg);
        txtv_msg.setText(getResources().getText(R.string.MessageAd)+myads.getBody());

        txtv_msgcnt = (TextView)findViewById(R.id.txtv_msgcnt);
        txtv_msgcnt.setText(getResources().getText(R.string.Messagecount)+myads.getMsg_limit());

        txtv_publisheddate = (TextView)findViewById(R.id.txtv_publisheddate);
        txtv_publisheddate.setText(getResources().getText(R.string.PublishedDateTime)+myads.getPublished_time());

        txtv_expiredate = (TextView)findViewById(R.id.txtv_expiredate);
        txtv_expiredate.setText(getResources().getText(R.string.ExpiredDateTime)+myads.getExpiry_time());

        txtv_msgperuser = (TextView)findViewById(R.id.txtv_msgperuser);
        txtv_msgperuser.setText(getResources().getText(R.string.PerPersonSent)+myads.getMsgs_per_user());

        txtv_msgsent = (TextView)findViewById(R.id.txtv_msgsent);
        txtv_msgsent.setText(getResources().getText(R.string.SentSMSCount)+myads.getCurrent_msg_count());

        txtv_msgdelivered = (TextView)findViewById(R.id.txtv_msgdelivered);
        txtv_msgdelivered.setText(getResources().getText(R.string.DeliveredSMSCount)+myads.getCurrent_deliver_count());

        txtv_payment = (TextView)findViewById(R.id.txtv_payment);
        txtv_payment.setText(getResources().getText(R.string.Chargepermsg)+myads.getPackege_msg_charge());

        txtv_paymentType = (TextView)findViewById(R.id.txtv_paymentType);
        txtv_paymentType.setText(getResources().getText(R.string.Totalcharge)+myads.getCurrent_charge_amount());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(MyAdsInfoActivity.this, MyAdsActivity.class);
        //i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }
}