package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.R;

/**
 * Created by dinithepieris on 9/18/17.
 */

public class ContactUsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        getSupportActionBar().setTitle(getResources().getString(R.string.contactus));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ContactUsActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }
}
