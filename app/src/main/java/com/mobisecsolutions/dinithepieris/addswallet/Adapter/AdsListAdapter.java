package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.PromoAdInfo;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class AdsListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PromoAdInfo> data;
    private static LayoutInflater inflater=null;


    public AdsListAdapter(Context context, ArrayList<PromoAdInfo> data) {
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(convertView == null)
            view = inflater.inflate(R.layout.list_row_ads, null);

        TextView title = (TextView)view.findViewById(R.id.txtv_title);
        TextView cost = (TextView)view.findViewById(R.id.txtv_cost);
        TextView expiredate = (TextView)view.findViewById(R.id.txtv_expiredate);
        TextView available_msg_count = (TextView)view.findViewById(R.id.txtv_msgavailable);
        //TextView msg_limit = (TextView)view.findViewById(R.id.txtv_msgLimit);
        //TextView current_msg_count = (TextView)view.findViewById(R.id.txtv_msgCurrent);

        PromoAdInfo promoAdInfo = data.get(position);

//        // Setting all values in listview
        title.setText("Promo sms:"+promoAdInfo.getBody());
        cost.setText("Per sms you can earn: " +promoAdInfo.getPayment() + "Rs");
        expiredate.setText("This ad will be expired on: " +promoAdInfo.getExpiry_date());
        //current_msg_count.setText("SMS sent : "+promoAdInfo.getCurrent_msg_count());
        //msg_limit.setText("SMS limit : "+promoAdInfo.getMsg_limit());
        int limit = Integer.parseInt(promoAdInfo.getMsg_limit());
        int current = Integer.parseInt(promoAdInfo.getCurrent_msg_count());
        int peruser= Integer.parseInt(promoAdInfo.getMsgs_per_user());
        //int deliver =  Integer.parseInt(promoAdInfo.getDeliver_count());
        if(current>peruser || current>limit || promoAdInfo.getIs_expired().equalsIgnoreCase("1")){
            available_msg_count.setText("Advertisement is expired now!");
            promoAdInfo.setIs_expired("1");
        }
        else if(limit>peruser) {
            available_msg_count.setText("SMS remaining : " + (peruser - current));
        }else{
            available_msg_count.setText("SMS remaining : " + (limit - current));
        }
        return view;
    }
}
