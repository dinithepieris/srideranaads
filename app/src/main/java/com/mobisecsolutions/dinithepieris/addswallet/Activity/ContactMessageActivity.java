package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.RecyclerMessageAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactMessageModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 9/27/17.
 */

public class ContactMessageActivity extends AppCompatActivity {

    //private SwipeRefreshLayout swipeContainer;

    ProgressDialog pDialog;
    String urlcontacthistory = "/api/contacthistory";
    String urlcontactus = "/api/contactus";
    Session session;//global variable

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerviewMessageList;

    ImageButton imgbtn_chat;
    EditText edttxt_chat;
    RecyclerMessageAdapter adapter;
    ArrayList <ContactMessageModel> msgList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_message);
        getSupportActionBar().setTitle(getResources().getString(R.string.leaveamsg));

        init();

//        // Lookup the swipe container view
//        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
//        // Setup refresh listener which triggers new data loading
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // Your code to refresh the list here.
//                // Make sure you call swipeContainer.setRefreshing(false)
//                // once the network request has completed successfully.
//                fetchTimelineAsync(0);
//            }
//        });
//        // Configure the refreshing colors
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);
    }


    private void init() {

        final int msglstcnt =10;

        session = new Session(ContactMessageActivity.this);
        recyclerviewMessageList = (RecyclerView) findViewById(R.id.recyclerviewMessageList);
        recyclerviewMessageList.setNestedScrollingEnabled(false);


        // Swipe Refresh Layout
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swifeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callLoadMessageTask(msglstcnt +10);
            }
        });
        //Make call to AsyncTask
        recyclerviewMessageList.stopScroll();
        callLoadMessageTask(msglstcnt);

        edttxt_chat = (EditText)findViewById(R.id.edttxt_chat);

        imgbtn_chat = (ImageButton)findViewById(R.id.imgbtn_chat);
        imgbtn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = edttxt_chat.getText().toString().trim();
                if(msg.length()>0) {
                    callContactMessageSendTask(msg);
                }
            }
        });
    }

    private void callContactMessageSendTask( String txt){
        String jsonReqObj = "";
        try {
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("message", txt);

            //convert JSONObject to JSON to String
            jsonReqObj = jsonObject.toString();

            String json = jsonObject.toString();
            new ContactMessageSendTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callLoadMessageTask( int cnt){
        String jsonReqObj = "";
        try {
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("count", cnt);

            //convert JSONObject to JSON to String
            jsonReqObj = jsonObject.toString();

            String json = jsonObject.toString();
            new loadMessageTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Login Async task class to get json by making HTTP call
     */
    private class loadMessageTask  extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
//            pDialog = new ProgressDialog(LoginActivity.this);
//            pDialog.setMessage("Please wait...");
//            pDialog.setCancelable(false);
//            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlcontacthistory;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            if (pDialog.isShowing()) {
//                pDialog.dismiss();
//            }
            if (result != null) {
                try {
                    JSONObject jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){

                        //ArrayList <ContactMessageModel> msgList = new ArrayList <ContactMessageModel>();
                        msgList = new ArrayList <ContactMessageModel>();
                        JSONArray msgs = jsonObj.getJSONArray("contact_history");

                        //looping through All Contacts
                        for (int i = 0; i < msgs.length(); i++) {
                            JSONObject c = msgs.getJSONObject(i);

                            String message = c.getString("message");
                            String message_time = c.getString("message_time");
                            String state = c.getString("state");
                            String reply = c.getString("reply");
                            String reply_time = c.getString("reply_time");

                            ContactMessageModel obj = new ContactMessageModel();

                            obj.setMessage(message);
                            obj.setMessage_time(message_time);
                            obj.setReply(reply);
                            obj.setReply_time(reply_time);
                            obj.setState(state);

                            msgList.add(0,obj);

                        }

                        if(msgList.size()>0) {

                            if (adapter == null) {
                                adapter = new RecyclerMessageAdapter(ContactMessageActivity.this, msgList);
                                //recyclerviewMessageList.setAdapter(adapter);

                                //mAdapter = new MoviesAdapter(movieList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                                recyclerviewMessageList.setLayoutManager(mLayoutManager);
                                recyclerviewMessageList.setItemAnimator(new DefaultItemAnimator());
                                recyclerviewMessageList.setAdapter(adapter);
                            }
                            //else{

                                adapter.notifyDataSetChanged();
                            //}
                            swipeRefreshLayout.setRefreshing(false);
                        }


                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {

                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(ContactMessageActivity.this, getResources().getString(R.string.app_name),
                        "Sorry! Server under maintain! \nPlease try again few later!");


            }
        }
    }


//    public void fetchTimelineAsync(int page) {
//        // Send the network request to fetch the updated data
//        // `client` here is an instance of Android Async HTTP
//        // getHomeTimeline is an example endpoint.
////        client.getHomeTimeline(new JsonHttpResponseHandler() {
////            public void onSuccess(JSONArray json) {
////                // Remember to CLEAR OUT old items before appending in the new ones
////                adapter.clear();
////                // ...the data has come back, add new items to your adapter...
////                adapter.addAll(...);
////                // Now we call setRefreshing(false) to signal refresh has finished
////                swipeContainer.setRefreshing(false);
////            }
////
////            public void onFailure(Throwable e) {
////                Log.d("DEBUG", "Fetch timeline error: " + e.toString());
////            }
////        });
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ContactMessageActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    /**
     *  chat Async task class to get json by making HTTP call
     */
    private class ContactMessageSendTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ContactMessageActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlcontactus;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);

                    if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(ContactMessageActivity.this, getResources().getString(R.string.app_name) , jsonObj.get("message").toString());
                    } else if(jsonObj.get("state").toString().equalsIgnoreCase("1")){

                        String message = edttxt_chat.getText().toString().trim();
                        edttxt_chat.setText("");
                        String message_time = Common.getDateTime();
                        String state = "state";
                        String reply = "reply";
                        String reply_time = "reply_time";

                        ContactMessageModel obj = new ContactMessageModel();

                        obj.setMessage(message);
                        obj.setMessage_time(message_time);
                        obj.setReply(reply);
                        obj.setReply_time(reply_time);
                        obj.setState(state);

                        msgList.add(obj);
                        adapter.notifyDataSetChanged();
                    }
                    else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogDirecttoLogin(ContactMessageActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(ContactMessageActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
            }
        }
    }
}
