package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.MyAdsModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 10/31/17.
 */

public class MyAdsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MyAdsModel> data;
    private static LayoutInflater inflater=null;


    public MyAdsAdapter(Context context, ArrayList<MyAdsModel> data) {
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row_myads, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        MyAdsModel currentListData = data.get(position);


        String publishState = "";

        if(!Boolean.valueOf(currentListData.getIs_expired())){
            switch(Integer.valueOf(currentListData.getPublish_state())) {
                case 1 :
                    publishState = "Pending";
                    break; // optional
                case 2 :
                    publishState = "Published";
                    break; // optional
                case 3 :
                    publishState = "Rejected";
                    break; // optional
            }
        }else{
            publishState = "Expired!";
        }

        mViewHolder.txtv_status.setText("Publish State : " +publishState);
        mViewHolder.txtv_msg.setText("Ad : " + currentListData.getBody());
        mViewHolder.txtv_msgcnt.setText("SMS count : "+currentListData.getMsg_limit());
        mViewHolder.txtv_publisheddate.setText("Published Date : "+currentListData.getPublished_time());
        mViewHolder.txtv_expiredate.setText("Expired Date : "+currentListData.getExpiry_time());
        mViewHolder.txtv_payment.setText("Charge per msg : "+currentListData.getPackege_msg_charge());
        mViewHolder.txtv_paymentType.setText("Total charge : "+currentListData.getCurrent_charge_amount());

        return convertView;
    }

    private class MyViewHolder {
        TextView txtv_status, txtv_msg, txtv_msgcnt, txtv_publisheddate, txtv_expiredate, txtv_payment, txtv_paymentType;

        public MyViewHolder(View item) {
            txtv_status = (TextView) item.findViewById(R.id.txtv_status);
            txtv_msg = (TextView) item.findViewById(R.id.txtv_msg);
            txtv_msgcnt = (TextView) item.findViewById(R.id.txtv_msgcnt);
            txtv_publisheddate = (TextView) item.findViewById(R.id.txtv_publisheddate);
            txtv_expiredate = (TextView) item.findViewById(R.id.txtv_expiredate);
            txtv_payment = (TextView) item.findViewById(R.id.txtv_payment);
            txtv_paymentType = (TextView) item.findViewById(R.id.txtv_paymentType);
        }
    }
}
