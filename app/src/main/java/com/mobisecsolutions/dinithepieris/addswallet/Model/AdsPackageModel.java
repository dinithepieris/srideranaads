package com.mobisecsolutions.dinithepieris.addswallet.Model;

import java.io.Serializable;

/**
 * Created by dinithepieris on 10/26/17.
 */

public class AdsPackageModel implements Serializable {
    private String id;
    private String name;
    private String amount;
    private String msg_limit;
    private String msgs_per_user;
    private String msg_charge;
    private String state;
    private String added_time;
    private String user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMsg_limit() {
        return msg_limit;
    }

    public void setMsg_limit(String msg_limit) {
        this.msg_limit = msg_limit;
    }

    public String getMsgs_per_user() {
        return msgs_per_user;
    }

    public void setMsgs_per_user(String msgs_per_user) {
        this.msgs_per_user = msgs_per_user;
    }

    public String getMsg_charge() {
        return msg_charge;
    }

    public void setMsg_charge(String msg_charge) {
        this.msg_charge = msg_charge;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAdded_time() {
        return added_time;
    }

    public void setAdded_time(String added_time) {
        this.added_time = added_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
