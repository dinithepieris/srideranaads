package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 8/23/17.
 */

public class UserRegistrationActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    EditText edt_fullname, edt_conntactNo, edt_email, edt_birthday, edt_gender,edt_addressLine1,
            edt_addressLine2, edt_city, edt_district, edt_password, edt_passwordConform;

    Button btn_cancel, btn_register;

    String urlDetails = "/api/signup";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        getSupportActionBar().setTitle(getResources().getString(R.string.register));
        init();
    }

    private void init() {

        edt_fullname = (EditText)findViewById(R.id.edt_fullname);
        edt_conntactNo = (EditText)findViewById(R.id.edt_conntactNo);
        edt_email = (EditText)findViewById(R.id.edt_email);
        edt_birthday = (EditText)findViewById(R.id.edt_birthday);
        edt_gender = (EditText)findViewById(R.id.edt_gender);
        edt_addressLine1 = (EditText)findViewById(R.id.edt_addressLine1);
        edt_addressLine2 = (EditText)findViewById(R.id.edt_addressLine2);
        edt_city = (EditText)findViewById(R.id.edt_city);
        edt_district = (EditText)findViewById(R.id.edt_district);
        edt_password = (EditText)findViewById(R.id.edt_password);
        edt_passwordConform = (EditText)findViewById(R.id.edt_passwordConform);

        edt_birthday.setInputType(InputType.TYPE_NULL);
        edt_gender.setInputType(InputType.TYPE_NULL);
        edt_district.setInputType(InputType.TYPE_NULL);

        btn_register = (Button) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValidate()){
                    try {
                        String json = "";
                        // build jsonObject
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.accumulate("msisdn", edt_conntactNo.getText().toString().trim());
                        jsonObject.accumulate("name", edt_fullname.getText().toString().trim());
                        jsonObject.accumulate("operator", "DIALOG");
                        jsonObject.accumulate("address", edt_addressLine1.getText().toString().trim()
                                +" "+edt_addressLine2.getText().toString().trim()
                                +" "+edt_city.getText().toString().trim());
                        jsonObject.accumulate("area", edt_district.getText().toString().trim());
                        jsonObject.accumulate("birthday", edt_birthday.getText().toString().trim());
                        jsonObject.accumulate("email", edt_email.getText().toString().trim());
                        jsonObject.accumulate("gender", edt_gender.getText().toString().trim());
                        jsonObject.accumulate("device_id", Common.getAndroidDeviceId(getApplicationContext()));
                        jsonObject.accumulate("password", edt_passwordConform.getText().toString().trim());

                        //convert JSONObject to JSON to String
                        json = jsonObject.toString();
                        if(Common.isNetworkAvailableCheck(UserRegistrationActivity.this)) {
                            new UserRegistarionTask().execute(json);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextIntent = new Intent(UserRegistrationActivity.this, LoginActivity.class);
                startActivity(nextIntent);
                finish();
            }
        });

        edt_district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] arrProvince = getResources().getStringArray(R.array.province);
                Common.listviewSelector(UserRegistrationActivity.this,arrProvince,edt_district);
            }
        });

        edt_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] gender = getResources().getStringArray(R.array.gender);
                Common.listviewSelector(UserRegistrationActivity.this,gender,edt_gender);
            }
        });

        edt_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateTimeField(edt_birthday);
            }
        });
    }

    private boolean isValidate(){
        String errormsg = "";
        if((edt_fullname.getText().toString().trim().length()< 3)
                ||(!isValidWord(edt_fullname.getText().toString().trim()))){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEnterName));
            return false;
        }
        if(edt_conntactNo.getText().toString().trim().length()!=10
                && edt_conntactNo.getText().toString().trim().charAt(0) == '0'
                && edt_conntactNo.getText().toString().trim().charAt(1) == '7'){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEntervalidContactNumber));
            return false;
        }
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(edt_email.getText().toString().trim()).matches()){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEntervalidEmail));
            return false;
        }
        if(edt_birthday.getText().toString().trim().length()< 3){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEnterBirthday));
            return false;
        }
        if(edt_gender.getText().toString().trim().length()< 3){
            Common.validationDialog(this,getResources().getString(R.string.PleaseSelectGender));
            return false;
        }
        if(edt_addressLine1.getText().toString().trim().length()< 3){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEnterAddress));
            return false;
        }
        if((edt_addressLine2.getText().toString().trim().length()< 3)
                ||(!isValidWord(edt_fullname.getText().toString().trim()))){

            Common.validationDialog(this,getResources().getString(R.string.PleaseEnterAddress));
            return false;
        }
        if((edt_city.getText().toString().trim().length()< 3)
                ||(!isValidWord(edt_fullname.getText().toString().trim()))){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEnterCity));
            return false;
        }

        if(edt_district.getText().toString().trim().length()< 3){
            Common.validationDialog(this,getResources().getString(R.string.PleaseSelectProvince));
            return false;
        }

        if(edt_password.getText().toString().trim().length()< 6){
            Common.validationDialog(this, getResources().getString(R.string.Pleaseentermorethan6letterpassword));
            return false;
        }

        if(!edt_passwordConform.getText().toString().trim().
                equals(edt_password.getText().toString().trim())){
            Common.validationDialog(this,getResources().getString(R.string.PasswordsMissedmatched));
            return false;
        }

        return  true;
    }

    public boolean isValidWord(String word) {
        return word.matches("[A-Za-z][^.]*");
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class UserRegistarionTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(UserRegistrationActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();
            // Making a request to url and getting response

            String strUrl = AppConfig.getBaseUrl() + urlDetails;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {
                        nextDialog( getResources().getString(R.string.Success),getResources().getString(R.string.YourdetailsSuccessfully));
                    }
                    else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(UserRegistrationActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(UserRegistrationActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
            }
        }
    }

    private void nextDialog(String title, String msg){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent nextIntent = new Intent(UserRegistrationActivity.this,
                                LoginActivity.class);
                        startActivity(nextIntent);
                        finish();
                    }
                });
        alertDialog.show();
    }
    private void setDateTimeField(final EditText editText) {

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setLenient(false);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editText.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        Common.backPressAlert(this);
    }
}

