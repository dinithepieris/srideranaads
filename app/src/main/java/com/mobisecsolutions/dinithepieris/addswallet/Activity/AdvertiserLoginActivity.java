package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.R;

/**
 * Created by dinithepieris on 8/31/17.
 */

public class AdvertiserLoginActivity extends AppCompatActivity {

    Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertiser_login);
        session = new Session(AdvertiserLoginActivity.this);

        Button btn = (Button)findViewById(R.id.btn_backtologin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setUserType(Common.PUBLISHER);
                Intent i = new Intent(AdvertiserLoginActivity.this, SplashActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
}
