package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 8/25/17.
 */

public class ContactListArrayAdapter extends ArrayAdapter<ContactModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<ContactModel> contactModelArrayList;
    boolean[] checkBoxState;

    public ContactListArrayAdapter(Context context, ArrayList<ContactModel> contact) {
        super(context,0, contact);
        this.context = context;
        this.contactModelArrayList = contact;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return contactModelArrayList.size();
    }

    @Override
    public ContactModel getItem(int position) {
        return contactModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return contactModelArrayList.get(position).hashCode();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_row_contact, null);

            holder = new ViewHolder();
            holder. name = (TextView)convertView.findViewById(R.id.txtv_name);
            holder. number = (TextView)convertView.findViewById(R.id.txtv_number);
            holder. chbxisSelected = (CheckBox)convertView.findViewById(R.id.chkbx_conSelect);
            holder.chbxisSelected.setTag( position);

            convertView.setTag(holder);

        }
        else{
            //Get viewholder we already created
            holder = (ViewHolder)convertView.getTag();
        }

        final ContactModel contactModel = contactModelArrayList.get(position);
        if(contactModel != null){
            holder.name.setText(contactModel.getName());
            holder.number.setText(contactModel.getMobileNumber());
            holder.chbxisSelected.setChecked(contactModel.getisSelected());

        }

//        holder. chbxisSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
//                //holder.chbxisSelected.setChecked(false);
//                contactModel.setisSelected(checked);
//
//                if(checked){
//                    SendAdsActivity.txtv_selecnumber.setText("Selected: "+ SendAdsActivity.ADS_selcted_cnt++);
//                }else{
//                    SendAdsActivity.txtv_selecnumber.setText("Selected: "+ SendAdsActivity.ADS_selcted_cnt--);
//                }
//            }
//        });

        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView name;
        TextView number;
        CheckBox chbxisSelected;
    }

}


