package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.Model.AdsPackageModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 11/6/17.
 */

public class AddCreditActivity extends AppCompatActivity {

    int CAMERA_REQUEST = 100;
    int GALLERY_REQUEST  = 200;


    EditText edtxt_creditAmount, edtxt_bank, edtxt_bank_branch, edtxt_refNo, edtxt_imdtContact;
    Button btn_SelectPaymentType, btn_upload, btn_pay, btn_back;
    TextView txtv_bank,txtv_bankAccNo, txtv_eznum, txtv_redeemval, txtv_balance;

    ProgressDialog pDialog;
    String urlaccountdetails= "/api/accountdetails";
    String urltopup = "/api/topup";

    JSONObject jsonObj;
    ArrayList<AdsPackageModel> adsPackage;

    int LOAD = 1;
    int CREDIT = 2;
    String SELECTED_PACKAGE;
    String PAY_TYPE ="";
    String IMAGESTR ="";

    String totalearn;

    LinearLayout llay_bankdetails, llay_cashdetails, llay_debitdetails;

    Session session;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_credit);
        getSupportActionBar().setTitle(getResources().getString(R.string.contactus));


        callGetPaymentAddCreditTask();

        init();


    }


    private void init() {

        session = new Session(AddCreditActivity.this);
        Common.setToken(session.getToken());

        llay_bankdetails = (LinearLayout) findViewById(R.id.llay_bankdetails);
        llay_cashdetails = (LinearLayout) findViewById(R.id.llay_cashdetails);
        llay_debitdetails = (LinearLayout) findViewById(R.id.llay_debitdetails);


        txtv_bank= (TextView) findViewById(R.id.txtv_bank);
        txtv_bankAccNo= (TextView) findViewById(R.id.txtv_bankAccNo);
        txtv_eznum = (TextView) findViewById(R.id.txtv_eznum);
        txtv_redeemval = (TextView) findViewById(R.id.txtv_redeemval);
        txtv_balance = (TextView) findViewById(R.id.txtv_balance);

        edtxt_bank = (EditText) findViewById(R.id.edtxt_bank);
        edtxt_bank_branch = (EditText) findViewById(R.id.edtxt_bank_branch);
        edtxt_refNo = (EditText) findViewById(R.id.edtxt_refNo);

        btn_upload = (Button)findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogSelectInput();
            }
        });

        edtxt_creditAmount = (EditText) findViewById(R.id.edtxt_creditAmount);
        edtxt_imdtContact = (EditText) findViewById(R.id.edtxt_imdtContact);

        btn_SelectPaymentType = (Button)findViewById(R.id.btn_SelectPaymentType);
        btn_SelectPaymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showPaymentMathodsDialog();
            }
        });

        btn_back = (Button)findViewById(R.id.btn_back);
        btn_pay = (Button)findViewById(R.id.btn_pay);
        //btn_pay.setEnabled(false);


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(AddCreditActivity.this, AdsMainActivity.class);
                i.putExtra("state",Common.APPONLINE);
                startActivity(i);
                finish();
            }
        });

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validatedata()) {
                    callAddCreditTask();
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(AddCreditActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    private boolean validatedata(){


        if(edtxt_creditAmount.getText().toString().trim().length()<1){
            Common.validationDialog(this,getResources().getString(R.string.PleaseentervalidAmount));
            return  false;
        }


        if((edtxt_imdtContact.getText().toString().trim().length()!=10)
                || (edtxt_imdtContact.getText().toString().trim().charAt(0) != '0')
                || (edtxt_imdtContact.getText().toString().trim().charAt(1) != '7')){
            Common.validationDialog(this,getResources().getString(R.string.PleaseEntervalidContactNumber));
            return false;
        }

        if(PAY_TYPE.trim().length() ==0){
            Common.validationDialog(this,getResources().getString(R.string.Pleaseselectpaymenttype));
            return false;
        }
        else if(PAY_TYPE.equalsIgnoreCase("1")) {
            IMAGESTR = null;
            if(edtxt_refNo.getText().toString().trim().length()<1){
                Common.validationDialog(this,getResources().getString(R.string.Pleaseenterreferencenumber));
                return  false;
            }
        }
        else if(PAY_TYPE.equalsIgnoreCase("4")) {
            if(edtxt_bank_branch.getText().toString().trim().length()<1){
                Common.validationDialog(this,getResources().getString(R.string.Pleaseenterbranchname));
                return  false;
            }
            if(IMAGESTR.length()<1){
                Common.validationDialog(this,getResources().getString(R.string.Pleaseuploadyourbanksliportransferdetailsimage));
                return  false;
            }

        }
        else if(PAY_TYPE.equalsIgnoreCase("5")) {
            if(edtxt_refNo.getText().toString().trim().length()<1){
                Common.validationDialog(this,getResources().getString(R.string.Pleaseenterreferencenumber));
                return  false;
            }
            IMAGESTR = null;
        }

        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (reqCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            //imageView.setImageBitmap(photo);
            IMAGESTR = getEncoded64ImageStringFromBitmap(image);
//            BitmapDrawable bdrawable = new BitmapDrawable(this.getResources(),image);
//            btn_upload.setBackground(bdrawable);
            btn_upload.setText(getResources().getText(R.string.Imagesuccessfullyuploaded));
        }

        else if (reqCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap image = BitmapFactory.decodeStream(imageStream);
                IMAGESTR = getEncoded64ImageStringFromBitmap(image);
                //image_view.setImageBitmap(image);

//                BitmapDrawable bdrawable = new BitmapDrawable(this.getResources(),image);
//                btn_upload.setBackground(bdrawable);
                btn_upload.setText(getResources().getText(R.string.Imagesuccessfullyuploaded));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(AddCreditActivity.this, getResources().getText(R.string.Somethingwentwrong), Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(AddCreditActivity.this, getResources().getText(R.string.YouhaventpickedanyImage),Toast.LENGTH_LONG).show();
        }
    }

    //String encodedImageData =getEncoded64ImageStringFromBitmap(your bitmap);

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public void showDialogSelectInput() {
        AlertDialog alertDialog = new AlertDialog.Builder(AddCreditActivity.this).create();
        alertDialog.setTitle(getApplicationContext().getResources().getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.Pleaseimportyourbankslip));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getText(R.string.CaptureImage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);

                        dialog.dismiss();

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getText(R.string.GalleryImage),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent, GALLERY_REQUEST);

                        dialog.dismiss();

                    }
                });
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    private void showPaymentMathodsDialog() {

        try {
            final JSONObject jsonObject = jsonObj.getJSONObject("account_details");

            AlertDialog.Builder builder = new AlertDialog.Builder(AddCreditActivity.this);
            builder.setTitle(getResources().getString(R.string.app_name));
            builder.setMessage(getString(R.string.SelectPaymentType));
            builder.setPositiveButton(getResources().getString(R.string.Bank),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            bankselectionDialog(jsonObject);
                            dialog.cancel();
                        }
                    });



            builder.setNegativeButton(getResources().getString(R.string.Eazycash),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            PAY_TYPE = "1";
                            try {
                                txtv_eznum.setText(getResources().getText(R.string.EazyCashNumber)+" : " + (jsonObject.get("eazycash")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            llay_bankdetails.setVisibility(View.GONE);
                            llay_cashdetails.setVisibility(View.VISIBLE);
                            llay_debitdetails.setVisibility(View.GONE);
                            dialog.cancel();
                        }
                    });

//            builder.setNeutralButton(" M - cash ",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            PAY_TYPE = "2";
//                            try {
//                                txtv_eznum.setText("M Cash Number : " + (jsonObject.get("mcash")));
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            llay_bankdetails.setVisibility(View.GONE);
//                            llay_cashdetails.setVisibility(View.VISIBLE);
//                            llay_debitdetails.setVisibility(View.GONE);
//                            dialog.cancel();
//                        }
//                    });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void bankselectionDialog(final JSONObject jsonObject){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( AddCreditActivity.this);
        alertDialogBuilder.setTitle("TopUp Request");
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.BankPayment))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.BankPayment), new DialogInterface.OnClickListener() {
                    public void onClick( DialogInterface dialog, int id) {
                        PAY_TYPE = "4";
                        try {
                            txtv_bank.setText(jsonObject.get("bank_name") + " " + jsonObject.get("branch_name"));
                            txtv_bankAccNo.setText(getResources().getText(R.string.DepositAccountNumber)+" : "+ jsonObject.get("acc_number"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        llay_bankdetails.setVisibility(View.VISIBLE);
                        edtxt_bank.setVisibility(View.GONE);
                        llay_cashdetails.setVisibility(View.GONE);
                        llay_debitdetails.setVisibility(View.GONE);
                        dialog.cancel();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.OnlineBank), new DialogInterface.OnClickListener() {
                    public void onClick( DialogInterface dialog, int id) {
                        PAY_TYPE = "4";
                        try {txtv_bank.setText(jsonObject.get("bank_name") + " " + jsonObject.get("branch_name"));
                            txtv_bankAccNo.setText(getResources().getText(R.string.DepositAccountNumber)+" : "+ jsonObject.get("acc_number"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        llay_bankdetails.setVisibility(View.VISIBLE);
                        edtxt_bank.setVisibility(View.VISIBLE);
                        llay_cashdetails.setVisibility(View.GONE);
                        llay_debitdetails.setVisibility(View.GONE);

                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    private void callAddCreditTask(){

        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());

            jsonObject.accumulate("amount", edtxt_creditAmount.getText().toString().trim());
            jsonObject.accumulate("payment_type", PAY_TYPE);
            jsonObject.accumulate("msisdn", edtxt_imdtContact.getText().toString().trim());
            jsonObject.accumulate("payment_bank", edtxt_bank.getText().toString().trim());
            jsonObject.accumulate("payment_branch", edtxt_bank_branch.getText().toString().trim());
            jsonObject.accumulate("ref_number", edtxt_refNo.getText().toString().trim());
            jsonObject.accumulate("bank_slip", IMAGESTR);


            //convert JSONObject to JSON to String
            json = jsonObject.toString();
            if (Common.isNetworkAvailableCheck(AddCreditActivity.this)) {
                new AddCreditTask(urltopup, CREDIT).execute(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private void callGetPaymentAddCreditTask(){



            try {
                String json = "";
                // build jsonObject
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("token", Common.getToken());

                //convert JSONObject to JSON to String
                json = jsonObject.toString();
                if (Common.isNetworkAvailableCheck(AddCreditActivity.this)) {
                    new AddCreditTask(urlaccountdetails, LOAD).execute(json);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }


    /**
     * Login Async task class to get json by making HTTP call
     */
    private class AddCreditTask extends AsyncTask<String, Void, String> {

        String Url;
        int request;

        public AddCreditTask(String url, int request){
            super();
            this.Url = url;
            this.request = request;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AddCreditActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + Url;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){

                        if(request == LOAD){
                            showPaymentMathodsDialog();
                        }else{
                            Common.showDialogDirect(AddCreditActivity.this, getResources().getString(R.string.app_name)
                                    , jsonObj.get("message").toString(), "AdsMainActivity");
                        }


                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {

                        Common.showDialogAlertOK(AddCreditActivity.this, getResources().getString(R.string.app_name)
                                , jsonObj.get("message").toString());

                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(AddCreditActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
            }
        }
    }
}
