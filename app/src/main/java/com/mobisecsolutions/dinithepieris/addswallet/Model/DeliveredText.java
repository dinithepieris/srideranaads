package com.mobisecsolutions.dinithepieris.addswallet.Model;

import java.io.Serializable;

/**
 * Created by dinithepieris on 9/6/17.
 */

public class DeliveredText implements Serializable {

    String phoneNumber;
    String adId;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }
}
