package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.AdsListAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Session;
import com.mobisecsolutions.dinithepieris.addswallet.Model.ContactModel;
import com.mobisecsolutions.dinithepieris.addswallet.Model.DeliveredText;
import com.mobisecsolutions.dinithepieris.addswallet.Model.PromoAdInfo;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class AdsMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ProgressDialog pDialog;
    String urlDetails = "/api/getnewads";
    String urlProfile = "/api/addeliveredcontactlist";

    //String filepath = "/mobisec/sriderana";
    //String filename = "ads.txt";

    ListView lst_adslst;

    TextView txtv_name, txtv_contact,txtv_earn,txtv_sent, txtv_delivered;

    ArrayList<PromoAdInfo> adsList;
    AdsListAdapter adsListAdapter;

    Session session;

    NavigationView navigationView;
    DrawerLayout drawer;

    JSONObject jobjProf;

    ImageButton imgbtn_myannouncement;
    RelativeLayout rl_myannouncement;
    TextView txt_myannouncement;

    String totalearn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /******sms******/
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Common.isNetworkAvailableCheck(AdsMainActivity.this)){
                    callProfileTask();
                }
                    //drawer.openDrawer(Gravity.END);
                    //drawer.openDrawer(Gravity.LEFT);
            }
        });

        getSupportActionBar().setTitle(getResources().getString(R.string.newads));

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                //appMode= null;
            } else {
                Common.setAppMode(extras.getInt("state"));
            }
        }
//        else {
//            Common.setAppMode((int) savedInstanceState.getSerializable("state"));
//        }

        session = new Session(AdsMainActivity.this);
        Common.setToken(session.getToken());

        if (session.getUserType().equalsIgnoreCase(Common.PUBLISHER)){
            navigationView.setCheckedItem(R.id.nav_publisher);
        }else if(session.getUserType().equalsIgnoreCase(Common.ADVERTISER)){
            navigationView.setCheckedItem(R.id.nav_advertiser);
        }

        init();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            Common.backPressAlert(AdsMainActivity.this);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ads_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            String[] gender = getResources().getStringArray(R.array.mylanguage);
            Common.listviewSelector(AdsMainActivity.this,gender);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        if(!drawer.isDrawerOpen(Gravity.LEFT)){
            callProfileTask();
        }

        int id = item.getItemId();

        if (id == R.id.nav_publisher) {
            navigationView.setCheckedItem(R.id.nav_publisher);
            //usertypeSelection();


        } else if (id == R.id.nav_advertiser) {
            navigationView.setCheckedItem(R.id.nav_publisher);
            //usertypeSelection();
            Intent intent = new Intent(AdsMainActivity.this, AdvertiseActivity.class);
            intent.putExtra("totalearn", totalearn);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_my_ads) {
            navigationView.setCheckedItem(R.id.nav_publisher);
            Intent intent = new Intent(AdsMainActivity.this, MyAdsActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_add_credit) {
            //navigationView.setCheckedItem(R.id.nav_publisher);
            Intent intent = new Intent(AdsMainActivity.this, AddCreditActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_payment) {
            //Toast.makeText(AdsMainActivity.this,"Payment methods will be implemented soon!",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(AdsMainActivity.this, PaymentActivity.class);
            try {
                intent.putExtra("name", jobjProf.get("name").toString().trim());
                intent.putExtra("number", jobjProf.get("msisdn").toString().trim());
                intent.putExtra("tearn", jobjProf.get("total_earned").toString().trim());
                intent.putExtra("tsmscnt", jobjProf.get("total_send_sms_count").toString().trim());
                intent.putExtra("tdsmscnt", jobjProf.get("total_delivered_sms_count").toString().trim());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            startActivity(intent);
            //finish();

        }else if (id ==R.id.nav_payment_history){
            Intent nextIntent = new Intent(AdsMainActivity.this, PaymentHistoryActivity.class);
            startActivity(nextIntent);
            //finish();
        } else if (id == R.id.nav_leaveamsg) {
            Intent nextIntent = new Intent(AdsMainActivity.this, ContactMessageActivity.class);
            startActivity(nextIntent);
            //finish();

        } else if (id == R.id.nav_contactus) {
            Intent nextIntent = new Intent(AdsMainActivity.this, ContactUsActivity.class);
            startActivity(nextIntent);
            //finish();

        } else if (id == R.id.nav_changepassword) {
            Intent nextIntent = new Intent(AdsMainActivity.this, ChangePasswordActivity.class);
            startActivity(nextIntent);
            //finish();

        } else if (id == R.id.nav_logout) {
            logoutSelection();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**********Implementation**********/
    private void init() {

        String lastAd = "0";
        adsList = new ArrayList<PromoAdInfo>();
        imgbtn_myannouncement = (ImageButton)findViewById(R.id.imgbtn_myannouncement);
        txt_myannouncement = (TextView) findViewById(R.id.txt_myannouncement);
        rl_myannouncement = (RelativeLayout)findViewById(R.id.rl_myannouncement);
        rl_myannouncement.setVisibility(View.GONE);

        lst_adslst = (ListView)findViewById(R.id.lstvw_adslst);
        lst_adslst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (adsList.get(i).getIs_expired().equalsIgnoreCase("1")) {
                    Toast.makeText(AdsMainActivity.this, getResources().getText(R.string.Thisadvertisementisexpired), Toast.LENGTH_LONG).show();
                } else if (adsList.get(i).getMsg_limit().equals(adsList.get(i).getCurrent_msg_count())) {
                    Toast.makeText(AdsMainActivity.this, getResources().getText(R.string.Advertisementsmscountisover), Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(AdsMainActivity.this, SendAdsActivity.class);
                    intent.putExtra("TxtAd", adsList.get(i).getBody());
                    intent.putExtra("TxtAdID", adsList.get(i).getId());
                    intent.putExtra("SMSCnt", Integer.parseInt(adsList.get(i).getMsg_limit())- Integer.parseInt(adsList.get(i).getCurrent_msg_count()));
                    startActivity(intent);
                    finish();
                }
            }
        });

        lst_adslst.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                rl_myannouncement.setVisibility(View.GONE);
                return false;
            }
        });


        imgbtn_myannouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_myannouncement.setVisibility(View.GONE);
            }
        });

        navigationView.setCheckedItem(R.id.nav_publisher);


//        if(Common.getAppMode() == Common.APPONLINE) {
//            if(!session.getAdId().equalsIgnoreCase("null")) {
//                lastAd = session.getAdId();
//            }

        if(Common.isNetworkAvailableCheck(this)){
            callLoadNewAdsTask(lastAd);
        }


//        }
//        else if(Common.getAppMode() == Common.APPOFFLINE) {
//            adsListAdapter = new AdsListAdapter(AdsWalletMainActivity.this, readArrayList());
//            lst_adslst.setAdapter(adsListAdapter);
//       }
    }

    private void callLoadNewAdsTask(String lastAd){
        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();

            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("last_recived_ad_id", lastAd);

            ArrayList<ContactModel> contactModelList = new ArrayList<ContactModel>();


//            if (session.getConLstCnt()!=contactModelList.size()){
//                session.setConLstCnt(contactModelList.size());

            JSONArray jsonArray = new JSONArray();

            //if (!Common.IS_CONTACTS_SYNC){
            //if (session.getTermsConditions().equalsIgnoreCase("Accepted")){

                contactModelList = Common.getMyContacts();
                if(contactModelList.size()!= Integer.valueOf( session.getConLstCnt())){
                    if(contactModelList.size()>0){
                        for (ContactModel contact : contactModelList) {

                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("name", contact.getName());
                            jsonObj.put("msisdn", contact.getMobileNumber());
                            jsonObj.put("email", contact.getEmail());
                            jsonArray.put(jsonObj);

                        }
                    }
                    session.setConLstCnt(contactModelList.size());
                }

                //Common.IS_CONTACTS_SYNC = true;
                session.delTermsConditions();
                session.setTermsConditions("Accepted_Synced");
            //}
            jsonObject.accumulate("sync_contact_list", jsonArray);

//            }
            //convert JSONObject to JSON to String
            json = jsonObject.toString();

            new LoadNewAdsTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Load New Ads Async task class to get json by making HTTP call
     */
    private class LoadNewAdsTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AdsMainActivity.this);
            pDialog.setMessage(getResources().getString(R.string.loadingnewads));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();
            // Making a request to url and getting response

            String strUrl = AppConfig.getBaseUrl() + urlDetails;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);

                    if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(AdsMainActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());
                        //txt_myannouncement.setVisibility(View.GONE);

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {

                        Common.AppVersionCheck(AdsMainActivity.this,jsonObj.getInt("appVersion"));
                        rl_myannouncement.setVisibility(View.VISIBLE);
                        txt_myannouncement.setText(jsonObj.get("message").toString());
                        // Getting JSON Array node
                        JSONArray ads = jsonObj.getJSONArray("new_ads");

                        //looping through All Contacts
                        for (int i = 0; i < ads.length(); i++) {
                            JSONObject c = ads.getJSONObject(i);

                            String id = c.getString("id");
                            String body = c.getString("body");
                            String added_time = c.getString("added_time");
                            String payment = c.getString("payment");
                            String state = c.getString("state");
                            String expiry_date = c.getString("expiry_date");
                            String is_expired = c.getString("is_expired");
                            String msg_limit = c.getString("msg_limit");
                            String current_msg_count = c.getString("current_msg_count");
                            String msgs_per_user = c.getString("msgs_per_user");
                            String deliver_count= c.getString("deliver_count");

                            PromoAdInfo adInfo = new PromoAdInfo();

                            adInfo.setId(id);
                            adInfo.setBody(body);
                            adInfo.setAdded_time(added_time);
                            adInfo.setPayment(payment);
                            adInfo.setState(state);
                            adInfo.setExpiry_date(expiry_date);
                            adInfo.setIs_expired(is_expired);
                            adInfo.setMsg_limit(msg_limit);
                            adInfo.setCurrent_msg_count(current_msg_count);
                            adInfo.setMsgs_per_user(msgs_per_user);
                            adInfo.setDeliver_count(deliver_count);

                            adsList.add(adInfo);

                        }

                        if(adsList.size()>0) {

//                            feedDatatoOfflineMode();
                            //session.setAdId(adsList.get(adsList.size()-1).getId());
                            adsListAdapter = new AdsListAdapter(AdsMainActivity.this, adsList);
                            lst_adslst.setAdapter(adsListAdapter);
                        }

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {
                        Common.showDialogAlertOK(AdsMainActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogDirecttoLogin(AdsMainActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(AdsMainActivity.this, getResources().getString(R.string.app_name),
                        getResources().getString(R.string.serverundermaintain));
            }
        }
    }


    private void callProfileTask(){
        try {
            String json = "";
            //build jsonObject
            JSONObject jsonObject = new JSONObject();

            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("load_profile", Common.LOADPROFILE);

            ArrayList<DeliveredText> deliveredList = new ArrayList<DeliveredText>();
            //deliveredList = Common.getDeliSmsList();
            //Session session = new Session(AdsMainActivity.this);
            deliveredList.addAll(0,session.getDeliveredList());

            if (deliveredList!= null && deliveredList.size()>0){

                JSONArray jsonArray = new JSONArray();

                for (DeliveredText delivereddetail : deliveredList) {

                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("ad_id", delivereddetail.getAdId());
                    jsonObj.put("msisdn", delivereddetail.getPhoneNumber());
                    jsonArray.put(jsonObj);

                }

                jsonObject.accumulate("delivered_list", jsonArray);
            }
            //convert JSONObject to JSON to String
            json = jsonObject.toString();

            new LoadProfileTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Load Profile Async task class to get json by making HTTP call
     */
    class LoadProfileTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(AdsMainActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Loadingprofile));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();
            // Making a request to url and getting response

            String strUrl = AppConfig.getBaseUrl() + urlProfile;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }

            JSONObject jsonObj;

            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {

                        session.delDeliveredList();

                        jobjProf = jsonObj.getJSONObject("profile");

                        txtv_name = (TextView)findViewById(R.id.txtv_name);
                        txtv_contact = (TextView)findViewById(R.id.txtv_contact);
                        txtv_earn = (TextView)findViewById(R.id.txtv_earn);
                        txtv_sent = (TextView)findViewById(R.id.txtv_sent);
                        txtv_delivered = (TextView)findViewById(R.id.txtv_delivered);

                        txtv_name.setText(jobjProf.get("name").toString().trim());
                        txtv_contact.setText(jobjProf.get("msisdn").toString().trim());
                        txtv_earn.setText(getResources().getString(R.string.Currentbalance)
                                + jobjProf.get("total_earned").toString().trim());
                        txtv_sent.setText(getResources().getString(R.string.TotalsntSMScount)
                                + jobjProf.get("total_send_sms_count").toString().trim());
                        txtv_delivered.setText(getResources().getString(R.string.TotaldeliveredSMScount)
                                + jobjProf.get("total_delivered_sms_count").toString().trim());

                        totalearn = jobjProf.get("total_earned").toString().trim();

                        drawer.openDrawer(Gravity.LEFT);

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(AdsMainActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("3")) {
                        Common.showDialogDirecttoLogin(AdsMainActivity.this, getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());

                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Common.showDialogAlertOK(AdsMainActivity.this, getResources().getString(R.string.app_name),
                        getResources().getString(R.string.serverundermaintain));
            }
        }
    }

    private void logoutSelection(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.areyousuretologout));
        builder.setMessage(getResources().getString(R.string.existinglocaldatawillbedeleted))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.Yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        session.delDeliveredList();
                        session.delAdId();
                        session.delConLstCnt();
                        session.delUserType();
                        session.delToken();
                        session.delTermsConditions();
                        Common.IS_CONTACTS_SYNC = false;
                        Intent intent = new Intent(AdsMainActivity.this, AdvertiserLoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

//    private void usertypeSelection(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(getResources().getString(R.string.Advertiser))
//                .setCancelable(false)
//                .setPositiveButton(getResources().getString(R.string.Advertiser), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        navigationView.setCheckedItem(R.id.nav_publisher);
//                        session.setUserType(Common.ADVERTISER);
//                        //Intent intent = new Intent(AdsMainActivity.this, AdvertiserLoginActivity.class);
//                        Intent intent = new Intent(AdsMainActivity.this, AdvertiseActivity.class);
//                        startActivity(intent);
//                        finish();
//                    }
//                })
//                .setNegativeButton(getResources().getString(R.string.Publisher), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        navigationView.setCheckedItem(R.id.nav_publisher);
//                        dialog.cancel();
//                    }
//                });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }

    /*******************Load Offline ******************************/

//    private void feedDatatoOfflineMode(){
//
//        writeArrayList(adsList);
//
//    }
//
//    private ArrayList<PromoAdInfo> readArrayList() {
//        ArrayList<PromoAdInfo> savedArrayList = null;
//
//        try {
//            //FileInputStream inputStream = new FileInputStream (new File(filename));
//            FileInputStream inputStream = openFileInput(filepath + "/"+ filename);
//            ObjectInputStream in = new ObjectInputStream(inputStream);
//            savedArrayList = (ArrayList<PromoAdInfo>) in.readObject();
//            in.close();
//            inputStream.close();
//
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        Toast.makeText(this,"DATA RETRIVED",Toast.LENGTH_LONG).show();
//        return savedArrayList;
//    }
//
//    private void writeArrayList(ArrayList<PromoAdInfo> arrayList) {
//        try {
//            File filedir = new File(filepath);
//            //filedir.getParentFile().mkdirs(); // Will create parent directories if not exists
//            final File file = new File(filedir, filename);
//            file.createNewFile();
//            //FileOutputStream fileOutputStream = openFileOutput(filepath, Context.MODE_PRIVATE);
//            FileOutputStream fileOutputStream = new FileOutputStream(file, false);
//            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
//            out.writeObject(arrayList);
//            out.close();
//            fileOutputStream.close();
//            Toast.makeText(this,"SAVED",Toast.LENGTH_LONG).show();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void deteleArrayListFile(){
//        File folder = Environment.getExternalStorageDirectory();
//        File myFile = new File(filepath+"/"+filename);
//        if(myFile.exists()) {
//            myFile.delete();
//            Toast.makeText(this,"DELETED",Toast.LENGTH_LONG).show();
//        }
//    }
}
