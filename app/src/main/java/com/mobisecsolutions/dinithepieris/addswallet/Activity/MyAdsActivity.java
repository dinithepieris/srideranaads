package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.MyAdsAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Model.MyAdsModel;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 10/30/17.
 */

public class MyAdsActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    String urlLogin = "/api/adpublishhistory";
    ListView lstv_myads;
    ArrayList<MyAdsModel> adsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ads);
        getSupportActionBar().setTitle(getResources().getString(R.string.myads));

        adsList = new ArrayList<MyAdsModel>();

        lstv_myads  = (ListView)findViewById(R.id.lstv_myads);

        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());

            //convert JSONObject to JSON to String
            json = jsonObject.toString();
            if(Common.isNetworkAvailableCheck(MyAdsActivity.this)) {
                new MyAdsLoaderTask().execute(json);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        lstv_myads.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MyAdsActivity.this, MyAdsInfoActivity.class);
                intent.putExtra("strjson", adsList.get(position));
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(MyAdsActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    /**
     * Login Async task class to get json by making HTTP call
     */
    private class MyAdsLoaderTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MyAdsActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlLogin;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("0")){

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("1")) {

                        JSONArray myads = jsonObj.getJSONArray("add_history");

                        //looping through All Contacts
                        for (int i = 0; i < myads.length(); i++) {
                            JSONObject c = myads.getJSONObject(i);

                            String id = c.getString("id");
                            String body = c.getString("body");
                            String is_expired = c.getString("is_expired");
                            String msg_limit = c.getString("msg_limit");
                            String msgs_per_user = c.getString("msgs_per_user");
                            String current_msg_count = c.getString("current_msg_count");
                            String current_deliver_count = c.getString("current_deliver_count");
                            String publish_state = c.getString("publish_state");
                            String published_time = c.getString("published_time");
                            String expiry_time = c.getString("expiry_time");
                            String packege_id = c.getString("packege_id");
                            String packege_name = c.getString("packege_name");
                            String packege_msg_limit = c.getString("packege_msg_limit");
                            String packege_msgs_per_user = c.getString("packege_msgs_per_user");
                            String packege_msg_charge = c.getString("packege_msg_charge");
                            String current_charge_amount = c.getString("current_charge_amount");

                            MyAdsModel adInfo = new MyAdsModel();

                            adInfo.setId(id);
                            adInfo.setBody(body);
                            adInfo.setIs_expired(is_expired);
                            adInfo.setMsg_limit(msg_limit);
                            adInfo.setMsgs_per_user(msgs_per_user);
                            adInfo.setCurrent_msg_count(current_msg_count);
                            adInfo.setCurrent_deliver_count(current_deliver_count);
                            adInfo.setPublish_state(publish_state);
                            adInfo.setPublished_time(published_time);
                            adInfo.setExpiry_time(expiry_time);
                            adInfo.setPackege_id(packege_id);
                            adInfo.setPackege_name(packege_name);
                            adInfo.setPackege_msg_limit(packege_msg_limit);
                            adInfo.setPackege_msgs_per_user(packege_msgs_per_user);
                            adInfo.setPackege_msg_charge(packege_msg_charge);
                            adInfo.setCurrent_charge_amount(current_charge_amount);

                            adsList.add(adInfo);
                        }

                        if(adsList.size()>0) {
                            MyAdsAdapter myAdsAdapter = new MyAdsAdapter(MyAdsActivity.this, adsList);
                            lstv_myads.setAdapter(myAdsAdapter);
                        }else{
                            Common.showDialogDirect(MyAdsActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.NoHistoryData), "AdsMainActivity");
                        }

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {
                        Common.showDialogDirect(MyAdsActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString(), "AdsMainActivity");
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(MyAdsActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));
            }
        }
    }
}