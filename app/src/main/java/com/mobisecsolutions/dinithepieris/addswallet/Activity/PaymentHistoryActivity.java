package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.mobisecsolutions.dinithepieris.addswallet.Adapter.PaymentHistoryListAdapter;
import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.Model.PaymentHistoryModel;

import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 9/26/17.
 */

public class PaymentHistoryActivity extends AppCompatActivity {

    Button btn_frombegining, btn_lastmonth, btn_lastweek;

    ProgressDialog pDialog;
    String urlgetpaymenthistory = "/api/getpaymenthistory";
    ListView lstv_payhistory;

    PaymentHistoryListAdapter payhistoryAdapter;
    ArrayList<PaymentHistoryModel> payHistroyList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);
        getSupportActionBar().setTitle(getResources().getString(R.string.paymenthistroy));
        init();
    }

    private void init() {

        lstv_payhistory = (ListView)findViewById(R.id.lstv_payhistory);

        btn_frombegining = (Button)findViewById(R.id.btn_frombegining);
        btn_frombegining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isNetworkAvailableCheck(PaymentHistoryActivity.this)) {
                    callPaymentHistory(36500);
                }
            }
        });

        btn_lastmonth = (Button)findViewById(R.id.btn_lastmonth);
        btn_lastmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isNetworkAvailableCheck(PaymentHistoryActivity.this)) {
                    callPaymentHistory(Common.DaysOfMonth);
                }
            }
        });

        btn_lastweek = (Button)findViewById(R.id.btn_lastweek);
        btn_lastweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isNetworkAvailableCheck(PaymentHistoryActivity.this)) {
                    callPaymentHistory(Common.DaysOfWeek);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(PaymentHistoryActivity.this, AdsMainActivity.class);
        i.putExtra("state", Common.APPONLINE);
        startActivity(i);
        finish();
    }

    private void callPaymentHistory(int dayscnt){
        try {
            String json = "";
            // build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("token", Common.getToken());
            jsonObject.accumulate("start_date",Common.getDateToday() );
            jsonObject.accumulate("end_date", Common.getDatePreviousday(dayscnt));

            //convert JSONObject to JSON to String
            json = jsonObject.toString();

            new PaymentHistoryTask().execute(json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class PaymentHistoryTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(PaymentHistoryActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlgetpaymenthistory;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){

                        JSONArray ads = jsonObj.getJSONArray("payment");
                        payHistroyList = new ArrayList<PaymentHistoryModel>();
                        // looping through All Contacts
                        for (int i = 0; i < ads.length(); i++) {
                            JSONObject c = ads.getJSONObject(i);

                            String id= c.getString("id");
                            String amount= c.getString("amount");
                            String appuser_id= c.getString("appuser_id");
                            String payment_type_id= c.getString("payment_type_id");
                            String request_msisdn= c.getString("request_msisdn");
                            String bank_details_id= c.getString("bank_details_id");
                            String bank_name= c.getString("bank_name");
                            String branch_name= c.getString("branch_name");
                            String acc_name= c.getString("acc_name");
                            String acc_number= c.getString("acc_number");
                            String state= c.getString("state");
                            String paid_by= c.getString("paid_by");
                            String added_time= c.getString("added_time");
                            String paid_time= c.getString("paid_time");
                            String can_see= c.getString("can_see");

                            PaymentHistoryModel payInfo = new PaymentHistoryModel();

                            payInfo.setId(id);
                            payInfo.setAmount(amount);
                            payInfo.setAppuser_id(appuser_id);
                            payInfo.setPayment_type_id(payment_type_id);
                            payInfo.setRequest_msisdn(request_msisdn);
                            payInfo.setBank_details_id(bank_details_id);
                            payInfo.setBank_name(bank_name);
                            payInfo.setBranch_name(branch_name);
                            payInfo.setAcc_name(acc_name);
                            payInfo.setAcc_number(acc_number);
                            payInfo.setState(state);
                            payInfo.setPaid_by(paid_by);
                            payInfo.setAdded_time(added_time);
                            payInfo.setPaid_time(paid_time);
                            payInfo.setCan_see(can_see);

                            payHistroyList.add(payInfo);
                        }

                        if(payHistroyList.size()>0) {
                            payhistoryAdapter = new PaymentHistoryListAdapter(PaymentHistoryActivity.this, payHistroyList);
                            lstv_payhistory.setAdapter(payhistoryAdapter);
                        }

                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("0")) {
                        Common.showDialogAlertOK(PaymentHistoryActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    } else if (jsonObj.get("state").toString().equalsIgnoreCase("2")) {
                        Common.showDialogAlertOK(PaymentHistoryActivity.this, getResources().getString(R.string.app_name), jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(PaymentHistoryActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));

            }
        }
    }
}
