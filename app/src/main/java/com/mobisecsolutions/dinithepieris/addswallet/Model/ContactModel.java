package com.mobisecsolutions.dinithepieris.addswallet.Model;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by dinithepieris on 8/24/17.
 */

public class ContactModel {

    public String id;
    public String name;
    public String mobileNumber;
    public String email;
    public Bitmap photo;
    public Uri photoURI;
    public boolean isSelected;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public Uri getPhotoURI() {
        return photoURI;
    }

    public void setPhotoURI(Uri photoURI) {
        this.photoURI = photoURI;
    }

    public boolean getisSelected() {
        return isSelected;
    }

    public void setisSelected(boolean isselected) {
        isSelected = isselected;
    }
}
