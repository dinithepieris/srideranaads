package com.mobisecsolutions.dinithepieris.addswallet.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mobisecsolutions.dinithepieris.addswallet.Common.AppConfig;
import com.mobisecsolutions.dinithepieris.addswallet.Common.Common;
import com.mobisecsolutions.dinithepieris.addswallet.R;
import com.mobisecsolutions.dinithepieris.addswallet.Service.HttpHandler;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;

/**
 * Created by dinithepieris on 9/18/17.
 */

public class PaymentActivity extends AppCompatActivity {

    TextView txt_ValsentSMS, txt_ValdeliveredSMS, txt_ValtotalEarn,txt_ValAvailableRedeem;
    Button btn_SelectPaymentType, btn_back, btn_pay;
    EditText edtxt_PaymentNum, edtxt_PaymentAmount,edtxt_bank, edtxt_bank_branch, edtxt_bank_name, edtxt_bank_accno;
    LinearLayout llay_bankdetails;
    ProgressDialog pDialog;
    String urlLogin = "/api/getpayment";
    int PaymentType = 0;
    String strname, strnumber, strtearn, strtdsmscnt,strsmscnt, stravlbal;
    ImageButton imgbtn_myannouncement;
    RelativeLayout rl_myannouncement;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                strname= null;
                strnumber= null;
                strtearn= null;
                strtdsmscnt= null;
                strsmscnt= null;
            } else {
                strname= extras.getString("name");
                strnumber= extras.getString("number");
                strtearn= extras.getString("tearn");
                strtdsmscnt= extras.getString("tdsmscnt");
                strsmscnt= extras.getString("tsmscnt");

            }
        } else {
            strname= (String) savedInstanceState.getSerializable("name");
            strnumber= (String) savedInstanceState.getSerializable("msisdn");
            strtearn= (String) savedInstanceState.getSerializable("total_earned");
            strsmscnt= (String) savedInstanceState.getSerializable("total_send_sms_count");
            strtdsmscnt= (String) savedInstanceState.getSerializable("total_delivered_sms_count");
        }

        init();

    }

    private void init(){

        edtxt_bank= (EditText) findViewById(R.id.edtxt_bank);
        edtxt_bank_branch= (EditText) findViewById(R.id.edtxt_bank_branch);
        edtxt_bank_name= (EditText) findViewById(R.id.edtxt_bank_name);
        edtxt_bank_accno= (EditText) findViewById(R.id.edtxt_bank_accno);

        txt_ValsentSMS = (TextView)findViewById(R.id.txt_ValsentSMS);
        txt_ValdeliveredSMS = (TextView)findViewById(R.id.txt_ValdeliveredSMS);
        txt_ValtotalEarn = (TextView)findViewById(R.id.txt_ValtotalEarn);
        txt_ValAvailableRedeem = (TextView)findViewById(R.id.txt_ValAvailableRedeem);

        imgbtn_myannouncement = (ImageButton)findViewById(R.id.imgbtn_myannouncement);
        rl_myannouncement = (RelativeLayout)findViewById(R.id.rl_myannouncement);

        txt_ValsentSMS.setText(strsmscnt);
        txt_ValdeliveredSMS.setText(strtdsmscnt);
        txt_ValtotalEarn.setText(strtearn);

        stravlbal = strtearn;
        txt_ValAvailableRedeem.setText(stravlbal);

        btn_SelectPaymentType = (Button)findViewById(R.id.btn_SelectPaymentType);
        btn_back = (Button)findViewById(R.id.btn_back);
        btn_pay = (Button)findViewById(R.id.btn_pay);
        btn_pay.setEnabled(false);

        edtxt_PaymentAmount= (EditText)findViewById(R.id.edtxt_PaymentAmount);
        edtxt_PaymentAmount.setKeyListener(DigitsKeyListener.getInstance(true,true));
        edtxt_PaymentNum = (EditText)findViewById(R.id.edtxt_PaymentNum);

        llay_bankdetails = (LinearLayout)findViewById(R.id.llay_bankdetails);

        btn_SelectPaymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPaymentMathodsDialog();
                rl_myannouncement.setVisibility(View.GONE);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PaymentActivity.this, AdsMainActivity.class);
                i.putExtra("state",Common.APPONLINE);
                startActivity(i);
                finish();
            }
        });

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Common.isNetworkAvailableCheck(PaymentActivity.this)) {
                    double amountpay = 0;
                    if(!edtxt_PaymentAmount.getText().toString().trim().matches("")) {
                        amountpay = Double.parseDouble(edtxt_PaymentAmount.getText().toString().trim());
                    }
                    int paytype = Integer.valueOf(PaymentType);

                    switch(paytype){
                        case 1:
                            if(amountpay>=250){
                                callPayDetails();
                                }
                            else{
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.enterpayEzyCash),Toast.LENGTH_LONG).show();

                            }
                            break;
                        case 2:
                            if(amountpay>=250){
                                callPayDetails();
                                }
                            else{
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.enterpayMCash),Toast.LENGTH_LONG).show();
                            }
                            break;
                        case 3:
                            if(amountpay>=100){
                                callPayDetails();
                                  }
                            else{
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.enterpayReload),Toast.LENGTH_LONG).show();
                            }
                            break;
                        case 4:
                            if(amountpay>=1000){
                                callPayDetails();
                                }
                            else{
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.enterpayBank),Toast.LENGTH_LONG).show();
                            }
                            break;
                    }


                }
            }
        });

        imgbtn_myannouncement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rl_myannouncement.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(PaymentActivity.this, AdsMainActivity.class);
        i.putExtra("state",Common.APPONLINE);
        startActivity(i);
        finish();
    }



    private void callPayDetails(){

        if((Double.parseDouble(stravlbal.trim()))<(Double.parseDouble(edtxt_PaymentAmount.getText().toString().trim()))){
            showDialogDirecthome(getResources().getString(R.string.app_name), getResources().getString(R.string.InvalidRedeemrequest));
        }else {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.accumulate("token", Common.getToken());
                jsonObject.accumulate("payment_type", PaymentType);
                jsonObject.accumulate("amount", edtxt_PaymentAmount.getText().toString().trim());

                if (PaymentType == 4) {
                    JSONObject jObject = new JSONObject();
                    jObject.accumulate("bank_name", edtxt_bank.getText().toString().trim());
                    jObject.accumulate("branch_name", edtxt_bank_branch.getText().toString().trim());
                    jObject.accumulate("acc_name", edtxt_bank_name.getText().toString().trim());
                    jObject.accumulate("acc_number", edtxt_bank_accno.getText().toString().trim());
                    jsonObject.accumulate("bank_details", jObject);

                } else {
                    jsonObject.accumulate("msisdn", edtxt_PaymentNum.getText().toString().trim());
                }
                String json = jsonObject.toString();
                new UserPaymentTask().execute(json);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class UserPaymentTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(PaymentActivity.this);
            pDialog.setMessage(getResources().getString(R.string.Pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {

            String jsonReqObj = params[0];

            HttpHandler handler = new HttpHandler();

            // Making a request to url and getting response
            String strUrl = AppConfig.getBaseUrl() + urlLogin;

            String jsonStr = handler.makeServiceCall(strUrl, jsonReqObj, "Post");

            Log.e(TAG, "Response from url: " + jsonStr);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObj;

            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (result != null) {
                try {
                    jsonObj = new JSONObject(result);
                    if(jsonObj.get("state").toString().equalsIgnoreCase("1")){
                        showDialogDirecthome(getResources().getString(R.string.app_name),"Redeem request Sent!");
                    }
                    else if(jsonObj.get("state").toString().equalsIgnoreCase("0")){
                        showDialogDirecthome(getResources().getString(R.string.app_name),
                                jsonObj.get("message").toString());
                    }

                } catch (final JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Common.showDialogAlertOK(PaymentActivity.this, getResources().getString(R.string.app_name),getResources().getString(R.string.serverundermaintain));

            }
        }
    }

    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }

    public void showDialogDirecthome(String title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent nextIntent = new Intent(PaymentActivity.this,AdsMainActivity.class);
                        nextIntent.putExtra("state", Common.APPONLINE);
                        startActivity(nextIntent);
                        finish();
                    }
                });
        alertDialog.show();

    }

    private void showPaymentMathodsDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
        builder.setTitle(getResources().getString(R.string.app_name));
        //builder.setIcon(R.drawable.icon);
        builder.setMessage(getResources().getString(R.string.SelectPaymentType));

        builder.setNeutralButton("Eazy Cash",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {

                        if(Double.parseDouble(stravlbal)>250.00){
                            btn_pay.setEnabled(true);

                            if(strnumber.length()==9){
                                strnumber = "0"+strnumber;
                            }
                            if((firstThree(strname).equalsIgnoreCase("071"))||(firstThree(strname).equalsIgnoreCase("070"))){
                                PaymentType = 2;
                                btn_SelectPaymentType.setText("Selected : M-Cash");
                            }else{
                                PaymentType = 1;
                                btn_SelectPaymentType.setText("Selected : Eazy Cash");
                            }

                            edtxt_PaymentNum.setText(strnumber);
                            llay_bankdetails.setVisibility(View.GONE);
                            edtxt_PaymentNum.setVisibility(View.VISIBLE);
                        }
                        else{
                            if((firstThree(strname).equalsIgnoreCase("071"))||(firstThree(strname).equalsIgnoreCase("070"))){
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.payEzyCash),Toast.LENGTH_LONG).show();
                            }else{
                                PaymentType = 1;
                                Toast.makeText(PaymentActivity.this,getResources().getString(R.string.payMCash),Toast.LENGTH_LONG).show();
                            }

                        }
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton("Reload",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        if(Double.parseDouble(stravlbal)>100.00){
                            btn_pay.setEnabled(true);
                            btn_SelectPaymentType.setText("Selected : Reload");
                            PaymentType = 3;
                            edtxt_PaymentNum.setText(strnumber);
                            llay_bankdetails.setVisibility(View.GONE);
                            edtxt_PaymentNum.setVisibility(View.VISIBLE);
                            dialog.cancel();
                        }
                        else{
                            Toast.makeText(PaymentActivity.this,getResources().getString(R.string.payReload),Toast.LENGTH_LONG).show();
                        }
                    }
                });

        builder.setPositiveButton("Bank",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        if(Double.parseDouble(stravlbal)>1000.00){
                            btn_pay.setEnabled(true);
                            btn_SelectPaymentType.setText("Selected : Bank");
                            PaymentType = 4;
                            edtxt_PaymentNum.setVisibility(View.GONE);
                            llay_bankdetails.setVisibility(View.VISIBLE);
                            dialog.cancel();
                    }
                    else{
                            Toast.makeText(PaymentActivity.this,getResources().getString(R.string.payBank),Toast.LENGTH_LONG).show();
                    }
                    }
                });
        builder.show();
    }
}
