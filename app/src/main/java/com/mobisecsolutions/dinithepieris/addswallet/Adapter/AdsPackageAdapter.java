package com.mobisecsolutions.dinithepieris.addswallet.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobisecsolutions.dinithepieris.addswallet.Model.AdsPackageModel;
import com.mobisecsolutions.dinithepieris.addswallet.Model.PromoAdInfo;
import com.mobisecsolutions.dinithepieris.addswallet.R;

import java.util.ArrayList;

/**
 * Created by dinithepieris on 10/26/17.
 */

public class AdsPackageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<AdsPackageModel> data;
    private static LayoutInflater inflater=null;


    public AdsPackageAdapter(Context context, ArrayList<AdsPackageModel> data) {
        this.context = context;
        this.data = data;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(convertView == null)
            view = inflater.inflate(R.layout.list_row_adspackage, null);

        TextView txtv_name = (TextView)view.findViewById(R.id.txtv_name);
        TextView txtv_msg_limit = (TextView)view.findViewById(R.id.txtv_msg_limit);
        TextView txtv_amount = (TextView)view.findViewById(R.id.txtv_amount);
        TextView txtv_msgs_per_user = (TextView)view.findViewById(R.id.txtv_msgs_per_user);
        TextView txtv_msgs_per_charge = (TextView)view.findViewById(R.id.txtv_msgs_per_charge);

        AdsPackageModel adsPackage = data.get(position);

        txtv_name.setText("Package :"+adsPackage.getName());
        txtv_msg_limit.setText("Total Message Count : " +adsPackage.getMsg_limit());
        txtv_amount.setText("Payment : " +adsPackage.getAmount());
        txtv_msgs_per_user.setText("Max count per user can send : " + adsPackage.getMsgs_per_user());
        txtv_msgs_per_charge.setText("Per message charge : " +adsPackage.getMsg_charge());

        return view;
    }
}

