package com.mobisecsolutions.dinithepieris.addswallet.Model;

/**
 * Created by dinithepieris on 10/11/17.
 */

public class TermsCondition {

    String header;
    String body;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
